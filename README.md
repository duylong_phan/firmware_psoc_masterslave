Firmware for dectection System for hose clamp pliers
----------------------------------------------------

In this project, there are 3 firmwares:

 - **PSOC4_Prototype**: Firmware for Master Device with PSOC4 Processor
 - **PSOC5_Prototype**: Firmware for Master Device with PSOC5 Processor
 - **PSOC4_Hose_Clamp_Pliers:** Firmware for Slave Device

In order to use the firmware, there are two method

# The project can be cloned in local computer with SourceTree Software. #

1. Select **Download** Option
1. Select **Download repository** Option
![Bitbuck_Clone.PNG](https://bitbucket.org/repo/BnkEG9/images/3720376948-Bitbuck_Clone.PNG)

# Or download the whole project as Zip File #

1. Select **Clone** Option
1. Select **Clone in SourceTree**, the SourceTree Software should already be installed in the Computer
![Bitbuck_Download.PNG](https://bitbucket.org/repo/BnkEG9/images/3931022052-Bitbuck_Download.PNG)

In order to program the firmware to the microncontroller, these tools are required:

* PSOC Creater from [Cypress](http://www.cypress.com/products/psoc-creator-integrated-design-environment-ide)
* USB Programmer

![Firmware_1.png](https://bitbucket.org/repo/BnkEG9/images/2828636494-Firmware_1.png)

After the project is cloned or downloaded to computer, the firmware project is ready to be used:

* Double click on **Firmware_PSOC_MasterSlave.cywrk** to start the PSOC Creator Software
![Firmware_StartProject.PNG](https://bitbucket.org/repo/BnkEG9/images/1827461123-Firmware_StartProject.PNG)

* Select the suitable firmware project to program, and click Program Icon
![Firmware_Program.PNG](https://bitbucket.org/repo/BnkEG9/images/501275360-Firmware_Program.PNG)

## Support ##
Please contact me via email:[phanduylong.gmai.com](Link URL)