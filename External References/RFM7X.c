#include "RFM7X.h"

//----------------------SPI Command----------------------
#define RFM7X_CMD_R_RX_PAYLOAD         		0x61         //read received Payload
#define RFM7X_CMD_W_TX_PAYLOAD         		0xA0         //write payload to transmit
#define RFM7X_CMD_FLUSH_TX             		0xE1         //Flush Tx buffer
#define RFM7X_CMD_FLUSH_RX             		0xE2         //Flush RX buffer
#define RFM7X_CMD_REUSE_TX_PL          		0xE3         //start continuous retransmission
#define RFM7X_CMD_W_TX_PAYLOAD_NOACK   		0xB0         //send without ack
#define RFM7X_CMD_W_ACK_PAYLOAD        		0xA8         //send with ack
#define RFM7X_CMD_ACTIVATE             		0x50         //toggle feature
#define RFM7X_CMD_R_RX_PL_WID          		0x60         //read received Length
#define RFM7X_CMD_NOP                  		0xFF         //read status
#define RFM7X_CMD_READ_REG             		0x00         //read flag for register
#define RFM7X_CMD_WRITE_REG            		0x20         //write flag for register
    
//-------------------Operation Register-----------------
#define RFM7X_REG_CONFIG               		0x00
#define RFM7X_REG_EN_AA                		0x01
#define RFM7X_REG_EN_RXADDR            		0x02
#define RFM7X_REG_SETUP_AW             		0x03
#define RFM7X_REG_SETUP_RETR           		0x04
#define RFM7X_REG_RF_CH                		0x05
#define RFM7X_REG_RF_SETUP             		0x06
#define RFM7X_REG_STATUS               		0x07
#define RFM7X_REG_OBSERVE_TX           		0x08
#define RFM7X_REG_CD                   		0x09
#define RFM7X_REG_RX_ADDR_P0           		0x0A
#define RFM7X_REG_RX_ADDR_P1           		0x0B
#define RFM7X_REG_RX_ADDR_P2           		0x0C
#define RFM7X_REG_RX_ADDR_P3           		0x0D
#define RFM7X_REG_RX_ADDR_P4           		0x0E
#define RFM7X_REG_RX_ADDR_P5           		0x0F
#define RFM7X_REG_TX_ADDR              		0x10
#define RFM7X_REG_RX_PW_P0             		0x11
#define RFM7X_REG_RX_PW_P1             		0x12
#define RFM7X_REG_RX_PW_P2             		0x13
#define RFM7X_REG_RX_PW_P3             		0x14
#define RFM7X_REG_RX_PW_P4             		0x15
#define RFM7X_REG_RX_PW_P5             		0x16
#define RFM7X_REG_FIFO_STATUS          		0x17
#define RFM7X_REG_DYNPD                		0x1C
#define RFM7X_REG_FEATURE              		0x1D

//-----------Interrupt status-------------------
#define RFM7X_Int_RX_DR                     0x40
#define RFM7X_Int_TX_DS                     0x20
#define RFM7X_Int_MAX_RT                    0x10

//------------FIFO_STATUS-----------------------
#define RFM7X_FIFO_TX_REUSE                 0x40
#define RFM7X_FIFO_TX_FULL                  0x20
#define RFM7X_FIFO_TX_EMPTY                 0x10
#define RFM7X_FIFO_RX_FULL                  0x02
#define RFM7X_FIFO_RX_EMPTY                 0x01

//---------------Bank Registration-----------------
//Operation Parameter
#define RFM7X_Bank0_Length              21
const unsigned char RFM7X_Bank0_Reg[RFM7X_Bank0_Length][2]=
{
    {0x00, 0x0F},           //Enable Rx mode, power up, CRC 2, all interupts
    {0x01, 0x3F},           //Enable auto Ack in all Pipe
    {0x02, 0x3F},           //Enable receive in all Pipe
    {0x03, 0x03},           //Enable 5 bytes addresses for Rx/Tx
    {0x04, RFM7X_Default_Retr},    
    {0x05, RFM7X_Default_Channel},
    {0x06, RFM7X_Default_RF_Setup}, 
    {0x07, 0x07},           //default as documents
    {0x08, 0x00},           //default as documents
    {0x09, 0x00},           //default as documents
    {0x0C, 0xC3},           //set Suffix Address Pipe2, 4 prefix byte as Pipe1
    {0x0D, 0xC4},           //set Suffix Address Pipe3, 4 prefix byte as Pipe1
    {0x0E, 0xC5},           //set Suffix Address Pipe4, 4 prefix byte as Pipe1
    {0x0F, 0xC6},           //set Suffix Address Pipe5, 4 prefix byte as Pipe1
    {0x11, 0x00},           //Number of byte in Pipe0 => dynamic
    {0x12, 0x00},           //Number of byte in Pipe1 => dynamic
    {0x13, 0x00},           //Number of byte in Pipe2 => dynamic
    {0x14, 0x00},           //Number of byte in Pipe3 => dynamic
    {0x15, 0x00},           //Number of byte in Pipe4 => dynamic
    {0x16, 0x00},           //Number of byte in Pipe5 => dynamic
    {0x17, 0x00}            //Reset FIFO status
};

//Store as Backup
#ifdef Com_RFM73
    //Chip Parameter
    const unsigned long RFM7X_Bank1_Reg[14] = 
    {
       0xE2014B40,          //0x00
       0x00004BC0,          //0x01
       0x028CFCD0,          //0x02
       0x41390099,          //0x03
       0x1B8296D9,          //0x04
       0xA67F0624,          //0x05
       0x00000000,          //0x06
       0x00000000,          //0x07
       0x00000000,          //0x08
       0x00000000,          //0x09
       0x00000000,          //0x0A
       0x00000000,          //0x0B
       0x00127300,          //0x0C
       0x36B48000           //0x0D
    };        

    //Last value of Bank1, separate for clear read
    const unsigned char RFM7X_Bank1_Reg14[11] =
    {
        0x41, 0x10, 0x04, 0x82, 0x20, 0x08, 0x08, 0xF2, 0x7D, 0xEF, 0xFF
    };
#endif

//Always use this, to compatible
#ifdef Com_RFM75
    //Chip Parameter
    const unsigned long RFM7X_Bank1_Reg[14] = 
    {
       0xE2014B40,          //0x00
       0x00004BC0,          //0x01
       0x028CFCD0,          //0x02
       0x41390099,          //0x03
       0xDB8A96F9,          //0x04
       0xB60F0624,          //0x05
       0x00000000,          //0x06
       0x00000000,          //0x07
       0x00000000,          //0x08
       0x00000000,          //0x09
       0x00000000,          //0x0A
       0x00000000,          //0x0B
       0x00127300,          //0x0C
       0x36B48000           //0x0D
    };        
    
    //Last value of Bank1, separate for clear read
    const unsigned char RFM7X_Bank1_Reg14[11] =
    {
        0x41, 0x20, 0x08, 0x04, 0x81, 0x20, 0xCF, 0xF7, 0xFE, 0xFF, 0xFF
    };
#endif

//------------Private Field--------------------
unsigned char RFM7X_Pin_SS = 0;
unsigned char RFM7X_Pin_CE = 0;
// Same address for Tx, Rx
unsigned char RFM7X_Default_Address_Tx[5] = {0x34, 0x43, 0x10, 0x10, 0x01};
unsigned char RFM7X_Default_Address_Rx[5] = {0x34, 0x43, 0x10, 0x10, 0x01};

//------------------------Private Method---------------------------
void RFM7X_Write_CE(unsigned char value)
{
    #if defined(PSOC4_Mode) || defined(PSOC5_Mode)
        RF_CE_Write(value);
    #endif
    
    #ifdef Arduino_Mode
        digitalWrite(RFM7X_Pin_CE, value);
    #endif  
}

void RFM7X_Write_SS(unsigned char value)
{
    #if defined(PSOC4_Mode) || defined(PSOC5_Mode)
        //HW automatic or implemented MasterSpi.h
    #endif
    
    #ifdef Arduino_Mode
        digitalWrite(RFM7X_Pin_SS, value);
    #endif 
}

void RFM7X_Write_Reg(unsigned char reg, unsigned char data)
{
	//Only for R/W register
    if(reg < RFM7X_CMD_WRITE_REG)
    {
        reg |= RFM7X_CMD_WRITE_REG;
    }	
    MasterSPI_Write_Reg(reg, data, RFM7X_Pin_SS);   
}

void RFM7X_Write_Reg_Buffer(unsigned char reg, const unsigned char *data, int length)
{
	 //Only for R/W register
    if(reg < RFM7X_CMD_WRITE_REG)
    {
        reg |= RFM7X_CMD_WRITE_REG;
    }
    MasterSPI_Write_Reg_Buffer(reg, data, length, RFM7X_Pin_SS);      
}

unsigned char RFM7X_Read_Reg(unsigned char reg)
{
    //Only for R/W register
    if(reg < RFM7X_CMD_READ_REG)
    {
        reg |= RFM7X_CMD_READ_REG;
    } 
    return MasterSPI_Read_Reg(reg, RFM7X_Pin_SS); 
}

void RFM7X_Read_Reg_Buffer(unsigned char reg, unsigned char *data, int length)
{
	 //Only for R/W register
    if(reg < RFM7X_CMD_READ_REG)
    {
        reg |= RFM7X_CMD_READ_REG;
    } 	
    MasterSPI_Read_Reg_Buffer(reg, data, length, RFM7X_Pin_SS);   
}

void RFM7X_Enable_BankIndex(unsigned char index)
{
    unsigned char value = RFM7X_Read_Reg(RFM7X_REG_STATUS);
    value &= 0x80;
    if((value && (index == 0)) ||
       ((value == 0) && index))
    {
        RFM7X_Write_Reg(RFM7X_CMD_ACTIVATE, 0x53);
    }
}

void RFM7X_InitializeBank()
{
    int i,k;
    unsigned char buffer[12];
    
    //Access Bank0
    RFM7X_Enable_BankIndex(0);
    for(i = 0; i < RFM7X_Bank0_Length; i++)
    {
        RFM7X_Write_Reg(RFM7X_Bank0_Reg[i][0], RFM7X_Bank0_Reg[i][1]);
    }    
    
    //Access Bank1
    RFM7X_Enable_BankIndex(1);
        
    //write Right to Left => Step1
    for(i = 0; i < 9;i++)
    {
        for(k = 0; k < 4; k++)
        {
            buffer[k] = (RFM7X_Bank1_Reg[i] >> (8*k)) & 0xFF;
        }
        RFM7X_Write_Reg_Buffer(i, buffer, 4);
    }
    
    //write Left to Right => Step2
    for(i = 9; i < 14; i++)
    {
        for(k = 0; k < 4; k++)
        {
            buffer[k] = (RFM7X_Bank1_Reg[i] >> (8*(3 - k))) & 0xFF;
        }
        RFM7X_Write_Reg_Buffer(i, buffer, 4);        
    }
    
    //Write last Reg => Step3
    RFM7X_Write_Reg_Buffer(14, RFM7X_Bank1_Reg14, 11); 
        
    //Write Reg 4 again => Step4
    for(i = 0; i < 4; i++)
    {
        buffer[i] = (RFM7X_Bank1_Reg[4] >> (8*i)) & 0xFF;
    }    
    buffer[0] |= 0x06;
    RFM7X_Write_Reg_Buffer(4,buffer,4);    
    buffer[0] &= 0xF9;
    RFM7X_Write_Reg_Buffer(4,buffer,4);
       
    //Access Bank0 => Important
    Sub_DelayMs(100);
    RFM7X_Enable_BankIndex(0);
}

//------------------------Public Method-----------------------------
//---------------Basic--------------------
unsigned char RFM7X_Start_Pin(unsigned char ce, unsigned char ss)
{
    RFM7X_Pin_CE = ce;
    RFM7X_Pin_SS = ss;
    return RFM7X_Start();
}

unsigned char RFM7X_Start()
{
    Sub_DelayMs(100);
    RFM7X_InitializeBank();
    
    RFM7X_SetAddress_Rx(RFM7X_Default_Address_Rx);
    RFM7X_SetAddress_Tx(RFM7X_Default_Address_Tx);
     
    if(RFM7X_Read_Reg(RFM7X_REG_FEATURE) == 0)
    {
        RFM7X_Write_Reg(RFM7X_CMD_ACTIVATE, 0x73);
    }    
    RFM7X_Write_Reg(RFM7X_REG_DYNPD, 0x3F);
    RFM7X_Write_Reg(RFM7X_REG_FEATURE, 0x07);
    RFM7X_Mode_Rx();
    
    return RFM7X_HasDevice();
}

unsigned char RFM7X_HasDevice()
{
    unsigned char hasDevice = 0;
    unsigned char tmpValue = RFM7X_Read_Reg(RFM7X_REG_RF_CH);
    
    //Check if parameter in Bank0 are correct written into Chip
    if(tmpValue == RFM7X_Default_Channel)
    {
        tmpValue = RFM7X_Read_Reg(RFM7X_REG_SETUP_RETR);
        if(tmpValue == RFM7X_Default_Retr)
        {
            hasDevice = 1;
        }
    } 
    return hasDevice;
}

//For passive checking, delay should be at lease 1ms, safe 2ms => stable, quick, but bad CPU
//For active checking with interrupt, is not recommended for Rx => unstable, slow, but good CPU
unsigned char RFM7X_HasData()
{    
    unsigned char tmpValue = RFM7X_Read_Reg(RFM7X_CMD_R_RX_PL_WID);
    tmpValue = (tmpValue > 0) ? 10 : 0;
    return tmpValue;
}

//---------------Status--------------------
void RFM7X_Mode_Rx()
{
    unsigned char tmpValue;
    
    //Clear Buffer, Interrupt
    RFM7X_Flush_Rx();    
    RFM7X_ClearInterrupt();
    
    //Mode Switch is enable
    RFM7X_Write_CE(0);
    
    //Update Register
    tmpValue = RFM7X_Read_Reg(RFM7X_REG_CONFIG);
    tmpValue |= 0x01;   //Enable Rx mode
    tmpValue |= 0x02;   //Power device up
    RFM7X_Write_Reg(RFM7X_REG_CONFIG, tmpValue);
    
    //Mode Switch is done 
    RFM7X_Write_CE(1);
}

void RFM7X_Mode_Tx()
{
    unsigned char tmpValue;
    
    //Clear Buffer    
    RFM7X_Flush_Tx(); 
    
    //Mode Switch is enable
    RFM7X_Write_CE(0);
    
    //Update Register
    tmpValue = RFM7X_Read_Reg(RFM7X_REG_CONFIG);    
    tmpValue &= 0xFE;   // clear RX bit
    tmpValue |= 0x02;   //Power device up
    RFM7X_Write_Reg(RFM7X_REG_CONFIG, tmpValue);
        
    //Mode Switch is done 
    RFM7X_Write_CE(1);
    Sub_DelayUs(20);
}

void RFM7X_PowerUp()
{
    unsigned char tmpValue;
    RFM7X_Write_CE(0);
    tmpValue = RFM7X_Read_Reg(RFM7X_REG_CONFIG);
    tmpValue |= 0x02;   //set PWR up bit
    RFM7X_Write_Reg(RFM7X_REG_CONFIG, tmpValue);
    RFM7X_Write_CE(1);
}

void RFM7X_PowerDown()
{
    unsigned char tmpValue;
    RFM7X_Write_CE(0);
    tmpValue = RFM7X_Read_Reg(RFM7X_REG_CONFIG);
    tmpValue &= 0xFD;   //clear PWR up bit
    RFM7X_Write_Reg(RFM7X_REG_CONFIG, tmpValue);
}

void RFM7X_GetStatus(unsigned char *isRx, unsigned char *isOn)
{
    unsigned char tmpValue = RFM7X_Read_Reg(RFM7X_REG_CONFIG);
    *isRx = (tmpValue & 0x01) ? 1 : 0;
    *isOn = (tmpValue & 0x02) ? 1 : 0;
}


//--------------Parameter setting------------------
void RFM7X_SetChannel(unsigned char channel)
{
    channel &= 0x7E;
    RFM7X_Write_Reg(RFM7X_REG_RF_CH, channel);
}

void RFM7X_SetAddress_Tx(const unsigned char *address)
{
    RFM7X_Write_Reg_Buffer(RFM7X_REG_TX_ADDR, address, RFM7X_Max_Address_Length);
}

void RFM7X_SetAddress_Rx(const unsigned char *address)
{
    RFM7X_Write_Reg_Buffer( RFM7X_REG_RX_ADDR_P0, address, RFM7X_Max_Address_Length);  
    RFM7X_Write_Reg_Buffer( RFM7X_REG_RX_ADDR_P1, address, RFM7X_Max_Address_Length); 
}

void RFM7X_SetAddress_Rx_Pipe(unsigned char index,unsigned char address)
{
    if(index > 1 && index < 6)
    {
        RFM7X_Write_Reg( RFM7X_REG_RX_ADDR_P0 + index, address); 
    }    
}

void RFM7X_SetPower(unsigned char power)
{
    unsigned char tmpValue = RFM7X_Read_Reg(RFM7X_REG_RF_SETUP);
    //clear bits
    tmpValue &= 0xF9;
    
    switch(power)
    {
        case RFM7X_Power_minus_10:
        tmpValue |= (0x00) << 1;
        break;
        
        case RFM7X_Power_minus_5:
        tmpValue |= (0x01) << 1;
        break;
        
        case RFM7X_Power_0:
        tmpValue |= (0x02) << 1;
        break;
        
        case RFM7X_Power_Plus_5:
        default:
        tmpValue |= (0x03) << 1;
        break;
    }
    RFM7X_Write_CE(0);
    RFM7X_Write_Reg(RFM7X_REG_RF_SETUP, tmpValue);
    RFM7X_Write_CE(1);
}

void RFM7X_SetDataRate(unsigned char speed)
{
    unsigned char tmpValue = RFM7X_Read_Reg(RFM7X_REG_RF_SETUP);
    //clear bits
    tmpValue &= 0xD7;
    switch(speed)
    {
        case RFM7X_250KBPS:
        tmpValue |= 1 << 5;
        break;
        
        case RFM7X_1MBPS:
        //take reset value
        break;
        
        case RFM7X_2MBPS:
        default:
        tmpValue |= 1 << 3;
        break;
    }
    RFM7X_Write_CE(0);
    RFM7X_Write_Reg(RFM7X_REG_RF_SETUP, tmpValue);
    RFM7X_Write_CE(1);
}


//--------------Transceiver Operation--------------
//Receive Packages, support for fixed and dynamic Package
unsigned char RFM7X_Receive(unsigned char *data, unsigned char *length, unsigned char *pipe)
{
    unsigned char hasData = RFM7X_Read_Reg(RFM7X_REG_STATUS);
      
    //Get Pipe
    hasData &= 0x0E;
    //Invalid Pipe => no Package
    if(hasData == 0x0E)
    {       
        *pipe = 0;
        *length = 0;   
         hasData = 0;
    }
    //Valid Pipe => Package is available
    else
    {
        //get package info
        *pipe = hasData;
        *length = RFM7X_Read_Reg(RFM7X_CMD_R_RX_PL_WID);
        
        //Avoid Invalid Length
        if(*length <= RFM7X_Max_Payload)
        {
            RFM7X_Read_Reg_Buffer(RFM7X_CMD_R_RX_PAYLOAD, data, *length);
            hasData = 1;
        }
    }
    
    //Important => Clear Interrupt if it is available
    RFM7X_ClearExistedInterrupt();
    
    return hasData;
}

void RFM7X_Transmit(const unsigned char *data, int length)
{
    //Important => Clear Interrupt if it is available
    RFM7X_ClearExistedInterrupt();
    
    //Transmit with Ack
    RFM7X_Write_Reg_Buffer(RFM7X_CMD_W_TX_PAYLOAD, data, length);
}

void RFM7X_Transmit_Once(const unsigned char *data, int length)
{
    //Important => Clear Interrupt if it is available
    RFM7X_ClearExistedInterrupt();
    
    //Transmit without Ack
    RFM7X_Write_Reg_Buffer(RFM7X_CMD_W_TX_PAYLOAD_NOACK, data, length);
}

//-------------Memory------------------------------
void RFM7X_Flush_Rx()
{
    RFM7X_Write_Reg(RFM7X_CMD_FLUSH_RX, 0);
}

void RFM7X_Flush_Tx()
{
    RFM7X_Write_Reg(RFM7X_CMD_FLUSH_TX, 0);
}

//-------------Interrupt---------------------------
void RFM7X_ClearInterrupt()
{
    RFM7X_Write_Reg( RFM7X_REG_STATUS , RFM7X_Read_Reg(RFM7X_REG_STATUS));
}

void RFM7X_ClearExistedInterrupt()
{
    unsigned char hasIn, hasOut, hasLost;
    RFM7X_GetInterrupt(&hasIn, &hasOut, &hasLost);
    if(hasIn || hasOut || hasLost)
    {
        RFM7X_ClearInterrupt();
    }
}

//Important => Interrupt only stop Transmision Mode, Received mode is continuous as long as FIFO is not full
//For real time checking, always add delay of [50,100] us for HW to update status
void RFM7X_GetInterrupt(unsigned char *hasIn, unsigned char *hasOut, unsigned char *hasLost)
{
    unsigned char tmpValue;    
    tmpValue = RFM7X_Read_Reg( RFM7X_REG_STATUS );
    *hasIn = tmpValue & RFM7X_Int_RX_DR;
    *hasOut = tmpValue & RFM7X_Int_TX_DS;
    *hasLost = tmpValue & RFM7X_Int_MAX_RT;
}