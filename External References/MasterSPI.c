#include "MasterSPI.h"

#ifdef MasterSPI_Native

    #ifdef PSOC4_Mode
        unsigned char lastSlavePin = -1;
        
        void MasterSPI_PSOC4_WriteBuffer(unsigned char *data, unsigned char length, unsigned char pin)
        {
            //important
            SPIM_ClearMasterInterruptSource(SPIM_INTR_MASTER_SPI_DONE);
            SPIM_SpiUartPutArray(data, length);
            while((SPIM_GetMasterInterruptSource() & SPIM_INTR_MASTER_SPI_DONE) == 0u);        
        }
    #endif

    //---------------------Basic---------------------
    unsigned char MasterSPI_WriteRead(unsigned char data)
    {
        unsigned char value;
        
        #ifdef PSOC4_Mode
            unsigned char array[1];
            array[0] = data;
            MasterSPI_PSOC4_WriteBuffer(array, 1, 0);
            value = SPIM_SpiUartReadRxData();
        #endif
        
        #ifdef PSOC5_Mode
            SPIM_WriteTxData(data);
            while((SPIM_ReadTxStatus() & SPIM_STS_SPI_DONE) == 0);
            value = SPIM_ReadByte();
        #endif
            
        #ifdef Arduino_Mode
            value = SPI.transfer(data);
        #endif
        
        return value;
    }

    void MasterSPI_Set_SS(unsigned char pin)
    {
        #if defined(PSOC4_Mode) && defined(MasterSPI_MultiSlave)
            if(pin != lastSlavePin)
            {
                lastSlavePin = pin;
                switch(pin)
                {
                    case 1:
                        SPIM_SpiSetActiveSlaveSelect(SPIM_SPI_SLAVE_SELECT1);
                    break;
                    case 2:
                        SPIM_SpiSetActiveSlaveSelect(SPIM_SPI_SLAVE_SELECT1);
                    break;
                    case 4:
                        SPIM_SpiSetActiveSlaveSelect(SPIM_SPI_SLAVE_SELECT1);
                    break;
                        
                    case 0:
                    default:
                        SPIM_SpiSetActiveSlaveSelect(SPIM_SPI_SLAVE_SELECT0);
                    break;            
                }  
                //Delay for HW to update
                Sub_DelayUs(10);
            }
        #endif
        
        #if defined(PSOC5_Mode) && defined(MasterSPI_MultiSlave)
            if(SS_Control_Read() != pin)
            {
                SS_Control_Write(pin);  
                //Delay for HW to update
                Sub_DelayUs(10);
            }
        #endif      
        
        #ifdef Arduino_Mode
            digitalWrite(pin, LOW);	
        #endif
    }

    void MasterSPI_Reset_SS(unsigned char pin)
    {
        #if defined(PSOC4_Mode) && defined(MasterSPI_MultiSlave)
            //hw automatic
        #endif
        
        #if defined(PSOC5_Mode) && defined(MasterSPI_MultiSlave)
            //hw automatic
        #endif
        
        #ifdef Arduino_Mode
            digitalWrite(pin, HIGH);	
        #endif
    }

    //----------------------Advance----------------------
    void MasterSPI_Write_Reg(unsigned char reg, unsigned char data, unsigned char pin)
    {
        unsigned char array[2] = {reg, data};     
        
        //set slave
        MasterSPI_Set_SS(pin);
        
        #ifdef PSOC4_Mode
            //write buffer
            MasterSPI_PSOC4_WriteBuffer(array, 2, 0); 
            //clear Rx buffer
            SPIM_SpiUartClearRxBuffer();
        #endif
        
        #ifdef PSOC5_Mode        
            //write buffer
            SPIM_PutArray(array, 2);
            //wait interrupt
        	while((SPIM_ReadTxStatus() & SPIM_STS_SPI_DONE) == 0)
            {
                //do nothing, wait for interrupt
            }
            //clear Rx buffer
            SPIM_ClearRxBuffer();
        #endif
            
        #ifdef Arduino_Mode 
            MasterSPI_WriteRead(array[0]);
            MasterSPI_WriteRead(array[1]);
        #endif
        
        //reset slave
        MasterSPI_Reset_SS(pin);
    }

    void MasterSPI_Write_Reg_Buffer(unsigned char reg, const unsigned char *data, int length, unsigned char pin)
    {
    	int i = 0;
        unsigned char array[128];
        
        //create buffer
    	array[0] = reg;
    	for(i = 0; i < length; i++)
    	{
    		array[i + 1] = data[i];
    	}
        
        //set slave
        MasterSPI_Set_SS(pin);
            
        #ifdef PSOC4_Mode
            //write reg + data
            MasterSPI_PSOC4_WriteBuffer(array, length + 1, 0); 
            //clear Rx buffer
            SPIM_SpiUartClearRxBuffer();
        #endif
        
        #ifdef PSOC5_Mode         
            //write reg + data
            SPIM_PutArray(array, length + 1);
            //wait interrupt
            while((SPIM_ReadTxStatus() & SPIM_STS_SPI_DONE) == 0)
            {
                //do nothing, wait for interrupt
            }
            //clear Rx buffer
        	SPIM_ClearRxBuffer();
        #endif    
        
        #ifdef Arduino_Mode     
            length++;
        	for (i = 0; i < length; i++)
        	{
        		MasterSPI_WriteRead(array[i]);
        	}    	
        #endif
        
        //reset slave
        MasterSPI_Reset_SS(pin);
    }

    unsigned char MasterSPI_Read_Reg(unsigned char reg, unsigned char pin)
    {
    	unsigned char value;
        #if defined(PSOC4_Mode) || defined(PSOC5_Mode)
            unsigned char array[2] = {reg, 0x00};
        #endif
        
        //set slave
        MasterSPI_Set_SS(pin);
        
        #ifdef PSOC4_Mode
            MasterSPI_PSOC4_WriteBuffer(array, 2, 0); 
            SPIM_SpiUartReadRxData();
            value = SPIM_SpiUartReadRxData();
        #endif
        
        #ifdef PSOC5_Mode  
            //write buffer
            SPIM_PutArray(array, 2);
            //wait interrupt
        	while((SPIM_ReadTxStatus() & SPIM_STS_SPI_DONE) == 0)
            {
                //do nothing, wait for interrupt
            }
            //discard first byte
            SPIM_ReadRxData();
            //read next
        	value = SPIM_ReadRxData();
        #endif    
        
        #ifdef Arduino_Mode       	
        	MasterSPI_WriteRead(reg);
        	value = MasterSPI_WriteRead(0x00);    	
        #endif
        
        MasterSPI_Reset_SS(pin);
    	return value;
    }

    void MasterSPI_Read_Reg_Buffer(unsigned char reg, unsigned char* data, int length, unsigned char pin)
    {
    	int i = 0;
        #if defined(PSOC4_Mode) || defined(PSOC5_Mode)
            unsigned char array[128];	
        	array[0] = reg;
        	for(i = 0; i < length; i++)
        	{
        		array[i + 1] = 0x00;
        	}
        #endif
        
        //set slave
        MasterSPI_Set_SS(pin);
            
        #ifdef PSOC4_Mode
            MasterSPI_PSOC4_WriteBuffer(array, length+1, 0); 
            //discard first byte
            SPIM_SpiUartReadRxData();  
            //read next bytes
            for(i = 0; i < length; i++)
        	{
        		data[i] = SPIM_SpiUartReadRxData();
        	}
        #endif
        
        #ifdef PSOC5_Mode
            //write buffer
            SPIM_PutArray(array, length + 1);
            //wait interrupt
        	while((SPIM_ReadTxStatus() & SPIM_STS_SPI_DONE) == 0)
            {
                //do nothing, wait for interrupt
            }
            //discard first byte
        	SPIM_ReadRxData();
            //read next bytes
            for(i = 0; i < length; i++)
        	{
        		data[i] = SPIM_ReadRxData();
        	}
        #endif    
        
        #ifdef Arduino_Mode   		
        	MasterSPI_WriteRead(reg);
        	for (i = 0; i < length; i++)
        	{
        		data[i] = MasterSPI_WriteRead(0x00);
        	}    	
        #endif
        
        //reset slave
        MasterSPI_Reset_SS(pin);
    }

#endif

//Pin has to be implemented in order to work properly
//Name conversion
/*-----PSOC-----
Mosi        SPI_MOSI
Miso        SPI_MISO
Clk         SPI_CLK

-------Arduino--
selbst implementation
*/
#ifdef MasterSPI_Pin
    
    //---------------------Basic---------------------
    unsigned char MasterSPI_WriteRead(unsigned char data)
    {
        int i;
        
        #if defined(PSOC4_Mode) || defined(PSOC5_Mode)
        for(i = 0; i < 8; i++)
        {
            Sub_DelayUs(1);
            if(data & 0x80)
            {
                SPI_MOSI_Write(1);   
            }
            else
            {
                SPI_MOSI_Write(0);
            }        
            
            Sub_DelayUs(1);
            SPI_CLK_Write(1);
            
            data  = data << 1;
            data |= SPI_MISO_Read();        
            
            Sub_DelayUs(2);
            SPI_CLK_Write(0);  
            SPI_MOSI_Write(0);
        }
        #endif
        
        return data;
    }

    void MasterSPI_Set_SS(unsigned char pin)
    {
        #if defined(PSOC4_Mode) || defined(PSOC5_Mode)
            RF_SS_Write(0);
        #endif
        
        #ifdef Arduino_Mode
            digitalWrite(pin, LOW);	
        #endif
    }

    void MasterSPI_Reset_SS(unsigned char pin)
    {
         #if defined(PSOC4_Mode) || defined(PSOC5_Mode)
            RF_SS_Write(1);
        #endif
        
        #ifdef Arduino_Mode
            digitalWrite(pin, HIGH);	
        #endif
    }

    //---------------------Advance---------------------
    void MasterSPI_Write_Reg(unsigned char reg, unsigned char data, unsigned char pin)
    {
        MasterSPI_Set_SS(pin);
        
        MasterSPI_WriteRead(reg);
        MasterSPI_WriteRead(data);
        
        MasterSPI_Reset_SS(pin);
    }

    void MasterSPI_Write_Reg_Buffer(unsigned char reg, const unsigned char *data, int length, unsigned char pin)
    {
        int i = 0;
        MasterSPI_Set_SS(pin);
        
        MasterSPI_WriteRead(reg);
        for(i = 0; i < length; i++)
        {
            MasterSPI_WriteRead(data[i]);
        }
        
        MasterSPI_Reset_SS(pin);
    }

    unsigned char MasterSPI_Read_Reg(unsigned char reg, unsigned char pin)
    {
        unsigned char value = 0;
        MasterSPI_Set_SS(pin);
        
        MasterSPI_WriteRead(reg);
        value = MasterSPI_WriteRead(0x00);
        
        MasterSPI_Reset_SS(pin);
        return value;
    }

    void MasterSPI_Read_Reg_Buffer(unsigned char reg, unsigned char* data, int length, unsigned char pin)
    {
        int i = 0;    
        MasterSPI_Set_SS(pin);
        
        MasterSPI_WriteRead(reg);
        for(i = 0; i < length; i++)
        {
            data[i] = MasterSPI_WriteRead(0x00);
        }
        
        MasterSPI_Reset_SS(pin);
    }
#endif