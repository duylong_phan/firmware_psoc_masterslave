#include <stdio.h>
#include <string.h>

#ifndef StreamBuffer_h
#define StreamBuffer_h

    struct StreamBuffer
    {
        //Raw array of address
        unsigned char *Container;
        //Max Amount of Item
        int MaxAmount;
        //Size of given Item in Bytes
        int TypeSize;
        //Important Index
        int StartIndex, MaxIndex, MinIndex;
        //Value Compare Method
        int (*CompareValue)(void *x, void *y);
    };

    //---------------------------Public Method---------------------------------
    /// <summary>
	/// Initialze StreamBuffer instance
	/// </summary>
	/// <param name="buffer">given instance</param>	
	/// <param name="container">data container</param>	
	/// <param name="maxAmount">max item amount</param>	
	/// <param name="typeSize">item size in byte</param>	
	/// <param name="func">item compare function</param>	
    void StreamBuffer_Initialize(struct StreamBuffer *buffer, unsigned char* container, int maxAmount, int typeSize, int (*func)(void *x, void *y));
	
    /// <summary>
	/// Add item in Buffer
	/// </summary>
	/// <param name="buffer">given channel</param>	
	/// <param name="item">new item</param>	
    void StreamBuffer_Add(struct StreamBuffer *buffer, void *item);
	
    /// <summary>
	/// Return item with given index
	/// </summary>
	/// <param name="buffer">given channel</param>	
	/// <param name="index">given index</param>	
    unsigned char* StreamBuffer_GetItem(struct StreamBuffer *buffer, int index);
	
    /// <summary>
	/// Get Index of Max item
	/// </summary>	
	/// <param name="channel">given channel</param>	
    int StreamBuffer_GetMaxIndex(struct StreamBuffer *buffer);
    
	/// <summary>
	/// Get Index of Min item
	/// </summary>
	/// <param name="buffer">given instance</param>	
    int StreamBuffer_GetMinIndex(struct StreamBuffer *buffer);
	
    /// <summary>
	/// Get offset index between original and target
	/// </summary>
	/// <param name="original">original index</param>	
	/// <param name="target">target index</param>	
    int StreamBuffer_GetOffset(int original, int target);
	
	/// <summary>
	/// Get next index from given index
	/// </summary>//Get Next index compare to given Index
	/// <param name="buffer">given instance</param>	
	/// <param name="index">given index</param>	
    int StreamBuffer_NextIndex(struct StreamBuffer *buffer, int index);
	
	/// <summary>
	/// Get last index from given index
	/// </summary>/Get Last index compare to given Index
	/// <param name="buffer">given instance</param>	
	/// <param name="index">given index</param>	
    int StreamBuffer_LastIndex(struct StreamBuffer *buffer, int index);
#endif
