#include "StreamBuffer.h"

//---------------------------Private Method---------------------------------

void StreamBuffer_SetItem(struct StreamBuffer *buffer, int index, void *item)
{    
    //Pointer Type as Container Pointer Type
    unsigned char *source = (unsigned char *)item;
    unsigned char *dest = StreamBuffer_GetItem(buffer, index);
    memcpy(dest, source, buffer->TypeSize);
}


//---------------------------Public Method---------------------------------
void StreamBuffer_Initialize(struct StreamBuffer *buffer, unsigned char* container, int maxAmount, int typeSize, int (*func)(void *x, void *y))
{
    buffer->Container = container;
    buffer->MaxAmount = maxAmount;
    buffer->TypeSize = typeSize;
    buffer->CompareValue = func;

    buffer->StartIndex = 0;
    buffer->MaxIndex = 0;
    buffer->MinIndex = 0;
}

void StreamBuffer_Add(struct StreamBuffer *buffer, void *item)
{
    //Overflow MaxAmount
    if(buffer->StartIndex >= buffer->MaxAmount)
    {
        buffer->StartIndex = 0;
    }

    //Set Item
    StreamBuffer_SetItem(buffer, buffer->StartIndex, item);
    
    //Find Max
    //new Value override old Max
    if(buffer->StartIndex == buffer->MaxIndex)
    {
        buffer->MaxIndex = StreamBuffer_GetMaxIndex(buffer);
    }
    //new value is new Max
    else if(buffer->CompareValue(item, StreamBuffer_GetItem(buffer, buffer->MaxIndex)) > 0)
    {
        buffer->MaxIndex = buffer->StartIndex;
    }

    //Find Min
    //new Value override old Min
    if(buffer->StartIndex == buffer->MinIndex)
    {
        buffer->MinIndex = StreamBuffer_GetMinIndex(buffer);
    }
    //new value is new Min
    else if(buffer->CompareValue(item, StreamBuffer_GetItem(buffer, buffer->MinIndex)) < 0)
    {
        buffer->MinIndex = buffer->StartIndex;
    }

    //Update StartIndex => Important
    buffer->StartIndex++;
}

int StreamBuffer_GetMaxIndex(struct StreamBuffer *buffer)
{
    int i;
    int maxIndex = 0;
    void *maxValue = StreamBuffer_GetItem(buffer,0);

    for(i = 0; i < buffer->MaxAmount; i++)
    {
        if(buffer->CompareValue( StreamBuffer_GetItem(buffer,i), maxValue) > 0)
        {
            maxIndex = i;
            maxValue = StreamBuffer_GetItem(buffer,i);
        }
    }

    return maxIndex;
}

unsigned char* StreamBuffer_GetItem(struct StreamBuffer *buffer, int index)
{
    return buffer->Container + index*buffer->TypeSize;
}

int StreamBuffer_GetMinIndex(struct StreamBuffer *buffer)
{
    int i;
    int minIndex = 0;
    void *minValue = StreamBuffer_GetItem(buffer,0);

    for(i = 0; i < buffer->MaxAmount; i++)
    {
        if(buffer->CompareValue( StreamBuffer_GetItem(buffer,i), minValue) < 0)
        {
            minIndex = i;
            minValue = StreamBuffer_GetItem(buffer,i);
        }
    }

    return minIndex;
}

int StreamBuffer_GetOffset(int original, int target)
{
    //Index as the original Point
    return target - original;
}

int StreamBuffer_NextIndex(struct StreamBuffer *buffer, int index)
{
    index++;
    return (index < buffer->MaxAmount) ? index : 0;
}

int StreamBuffer_LastIndex(struct StreamBuffer *buffer, int index)
{
    index--;
    return (index < 0) ? (buffer->MaxAmount - 1) : index;
}