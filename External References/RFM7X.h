#include "MasterSPI.h"
    
#ifndef RFM7X_h
#define RFM7X_h     
    /*Important Step
    1. Test connection with multimeter
    2. Test Power supply [1.9, 3.6]
    3. Test Device available width RFM7X_HasDevice
    4. Test Communication with RFM7X_HasData
    */
    
    //---------------Public definition-------------------
    #define RFM7X_Max_Payload               32 
    #define RFM7X_Max_Address_Length        5 
    #define RFM7X_250KBPS			        0x00
    #define RFM7X_1MBPS				        0x01
    #define RFM7X_2MBPS				        0x02
    #define RFM7X_Power_minus_10            0x00
    #define RFM7X_Power_minus_5	            0x01
    #define RFM7X_Power_0 	                0x02
    #define RFM7X_Power_Plus_5       	    0x03
    // auto retransmission delay 1ms, 15 times
    #define RFM7X_Default_Retr              0xFF    
    // data rate 250Kbps, power 5dbm, LNA gain high
    #define RFM7X_Default_RF_Setup          0x27    
    // channel 20
    #define RFM7X_Default_Channel           0x14
        
    //---------------Basic--------------------
	/// <summary>
	/// Start and return value if module is successfully started, only for Arduino
	/// </summary>
    unsigned char RFM7X_Start_Pin(unsigned char ce, unsigned char ss);
	/// <summary>
	/// Start and return value if module is successfully started
	/// </summary>
    unsigned char RFM7X_Start();
	/// <summary>
	/// Check if Device is correctly connected, only with simple Start()
	/// </summary>
    unsigned char RFM7X_HasDevice();
	/// <summary>
	/// Check if Receive Package is available
	/// </summary>
    unsigned char RFM7X_HasData();
    
    //---------------Status--------------------
	/// <summary>
	/// Switch to Receive Mode
	/// </summary>
    void RFM7X_Mode_Rx();
	/// <summary>
	/// Switch to Transmit Mode
	/// </summary>
    void RFM7X_Mode_Tx();    
	/// <summary>
	/// Turn on the Module
	/// </summary>
    void RFM7X_PowerUp();
	/// <summary>
	/// Turn off the Module
	/// </summary>
    void RFM7X_PowerDown();
	/// <summary>
	/// Check Module Status
	/// </summary>
	/// <param name="isRx">Device in Rx</param>	
	/// <param name="isOn">Device is on</param>	
    void RFM7X_GetStatus(unsigned char *isRx, unsigned char *isOn);
    
    //--------------Parameter setting------------------
	/// <summary>
	/// Set Communication Channel
	/// </summary>
	/// <param name="channel">given channel</param>	
    void RFM7X_SetChannel(unsigned char channel);
	/// <summary>
	/// Set Communication Transmission Address
	/// </summary>
	/// <param name="address">given address</param>	
    void RFM7X_SetAddress_Tx(const unsigned char *address);
	/// <summary>
	/// Set Communication Receive Address for Pipe0, Pipe1
	/// </summary>
	/// <param name="address">given address</param>	
    void RFM7X_SetAddress_Rx(const unsigned char *address);    
	/// <summary>
	/// Set Communication Receive Address for specific Pipe
	/// </summary>
	/// <param name="miliSecond">given miliSecond</param>	
	/// <param name="miliSecond">given miliSecond</param>	
    void RFM7X_SetAddress_Rx_Pipe(unsigned char index,unsigned char address);  
	/// <summary>
	/// Set Transceiver Power
	/// </summary>
	/// <param name="power">given power</param>	
    void RFM7X_SetPower(unsigned char power);
	/// <summary>
	/// Set Transceiver Datarate
	/// </summary>
	/// <param name="speed">given speed</param>	
    void RFM7X_SetDataRate(unsigned char speed);
    
    //--------------Transceiver Operation--------------    
	/// <summary>
	/// Write container and transmit
	/// </summary>
	/// <param name="data">data container</param>	
	/// <param name="length">container length</param>	
    void RFM7X_Transmit(const unsigned char *data, int length);
	/// <summary>
	/// Write container and transmit without retransmission
	/// </summary>
	/// <param name="data">data container</param>	
	/// <param name="length">container length</param>	
    void RFM7X_Transmit_Once(const unsigned char *data, int length);
	/// <summary>
	/// Read the Package from Module, and its parameter
	/// </summary>
	/// <param name="data">data container</param>	
	/// <param name="length">package length</param>	
	/// <param name="pipe">package pipe</param>	
    unsigned char RFM7X_Receive(unsigned char *data, unsigned char *length, unsigned char *pipe);
    
    //-------------Memory------------------------------
	/// <summary>
	/// Clear all data in receive Buffer
	/// </summary>
    void RFM7X_Flush_Rx();
	/// <summary>
	/// Clear all data in transmit Buffer
	/// </summary>
    void RFM7X_Flush_Tx();
    
    //-------------Interrupt---------------------------
	/// <summary>
	/// Clear Module interrupt status
	/// </summary>
    void RFM7X_ClearInterrupt();    
	/// <summary>
	/// Clear Module interrupt status, only interrupt is available
	/// </summary>
    void RFM7X_ClearExistedInterrupt();
	/// <summary>
	/// Get module interrupt
	/// </summary>
	/// <param name="hasIn">received Package is available</param>	
	/// <param name="hasOut">package is transmitted</param>	
	/// <param name="hasLost">package is lost</param>	
    void RFM7X_GetInterrupt(unsigned char *hasIn, unsigned char *hasOut, unsigned char *hasLost);    
#endif