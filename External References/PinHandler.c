#include "PinHandler.h"

void PinHandler_Initialize(struct PinHandler *handler, unsigned char id, void (*write)(unsigned char), unsigned char (*read)())
{
    handler->ID = id;    
    handler->Count = 0;
    handler->Command = 0;
    handler->Write = write;
    handler->Read = read;
}