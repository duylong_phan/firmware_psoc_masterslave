#ifndef Definitions_h
#define Definitions_h

	//----------------Operation State----------------------    
    #define State_Wait_Input                1
    #define State_RF_Interrupt              2
    #define State_RF_Receive                3
    #define State_RF_Transmit               4
    #define State_Task                      5
    #define State_Process_Input             6
    #define State_Generate_Output           7
       
        
    //-------------------Common-----------------------------
    //Buffer size
    #define Size_RF_Package                 32
    #define Size_RF_PayLoad                 30
    #define Size_StateContainer             20  
    #define Size_TransmitContainer          20
    #define Size_HeaderContainer            64
    #define Size_SettingBuffer              58
    #define Size_SlaveAmount                255
    
    //Operation mode
    #define Oper_Normal                     0x00
    #define Oper_Debug                      0x80

    //Task status
    #define Task_Disable                    0
    #define Task_Enable                     1
    
    //Timer
    #define TickCount_Second                1000
    #define TickCount_Minute                60000
    #define TickCount_Hour                  3600000
    
    //RF operation
    #define RF_Off                          0
    #define RF_Receiver                     1
    #define RF_Transmitter                  2
    #define RF_SwitchWait                   10          //in ms
    #define Max_LostCount                   2    
    
    
    //PC Command Type
    #define PC_Error                        0
    #define PC_Start                        1
    #define PC_Stop                         2
    #define PC_RF_Transmit                  3
    #define PC_Command_Done                 4
    #define PC_Read_MasterSetting           5
    #define PC_Write_MasterSetting          6
    #define PC_Read_SlaveSetting            7
    #define PC_Write_SlaveSetting           8
    #define PC_PinWrite                     9
    #define PC_PinRead                      10
	#define PC_Press_Pressed                50
	#define PC_Press_Sample                 51
    #define PC_Press_Status                 52    
	#define PC_Cap_Done                     60
	#define PC_uBalance_Status				61
    #define PC_RfTrans_Status               62
    #define PC_RfTrans_Count                63
    #define PC_MotorCtr_Status              64    
    #define PC_MotorCtr_DataSample          65
    
    
    
    
    //PC Actions
    #define PC_Action_Bit_AppStatus         0
    #define PC_Action_Bit_InfoMessage       1
    #define PC_Action_Bit_WarningMessage    2
    #define PC_Action_Bit_ErrorMessage      3

    //-----------------------Header Transceiver Package------------------    
    //PC Package
    #define Index_PC_Command                0
    #define Index_PC_Size                   1
    
    //RF Package
    #define Index_TaskID                    0
    #define Index_Type                      1
    #define Index_Payload                   2  
    
    //Task ID
    #define TaskID_Dummy                    0
    #define TaskID_Press                    1
    #define TaskID_CapMeasure               2
    #define TaskID_AccMeasure               3
	#define TaskID_uBalance					4
    #define TaskID_RfTrans                  5
    #define TaskID_MotorCtr                 6
    
    //-----------------------Master Specific--------------------------------
    #define Setting_Master_RF_Channel       0
    #define Setting_Master_RF_Rx_Address    1
    #define Setting_Master_RF_Tx_Address    6
	
	//-----------------------Slave Task Specific-----------------------------
      
	//Press
    #define Type_Press_Non                  0x00
	#define Type_Press_DeviceStatus         0x01
    #define Type_Press_Pressed              0x02
    #define Type_Press_Debug                0x04
    #define Type_Press_ReadSetting          0x08
    #define Type_Press_WriteSetting         0x10
        
    #define Status_Bit_Press_Available      0
    #define Status_Bit_Press_SampleOn       1
    #define Status_Bit_Press_SampleOff      2
    
    #define Pos_Press_ID                    2
    #define Pos_Press_Amount                3
    #define Pos_Press_Sample                4
    #define Pos_Press_PcAction              3
    #define Pos_Press_Status                7
    #define Pos_Press_SettingIndex          2
    #define Pos_Press_SettingContent        3
    #define Size_Press_Sample               20
    #define Size_Press_SettingPackage       29   
    #define Pos_Press_PackageIndex          31
    
    //---------------------Struct Union-------------------------
    #define PSOC5_USB_Size_Package          64
    #define PSOC5_USB_Size_PayLoad          63
    #define PSOC5_USB_Index_Length          63
    
    //Important => Always SPI Speed <= 500kps
    struct RfHandler
    {
        unsigned char TransmitPackage[Size_RF_Package];
        unsigned char ReceivePackage[Size_RF_Package];
        //Current RF Mode
        unsigned char Mode;
        //Current received Pipe Index
        unsigned char Pipe;
        //Current Package length
        unsigned char Length;    
        //Check Received Package in RF => use in Real-Time Task
        unsigned char CheckInput;
        //Wait in ms, before transmit package to RF => use in Real-Time Task
        unsigned short WaitOutput;
        //Lost Amount, for firmware retransmit
        short LostCount;
    };
    
    struct UsbHandler
    {
        unsigned char TransmitPackage[PSOC5_USB_Size_Package];
        unsigned char ReceivePackage[PSOC5_USB_Size_Package];
        unsigned char *Length;
    };
    
    //---------------------Initialize Method--------------------
    void Initialize_HW();
    void Initialize_SW();
    void Initialize_Parameters();
    void Single_Operation();    
    
    //--------------------Interrupt Method----------------------
    void On_RF_Inter();
    void On_Timer_Inter();    
    
    //------------------Operation Methods-----------------------    
    void Do_Wait_Input();    
    void Do_RF_Interrupt();
    void Do_RF_Transmit();
    void Do_RF_Receive();
    void Do_Task();
    void Do_Process_Input();
    void Do_Generate_Output();
    
    //------------------others function--------------------------
    unsigned char HasInput();    
#endif