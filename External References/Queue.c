#include "Queue.h"

//---------------------------Private Method---------------------------------
unsigned char* Queue_GetItem(struct Queue *queue, int index)
{
    return queue->Container + index*queue->TypeSize;
}

void Queue_SetItem(struct Queue *queue, int index, void *item)
{    
    //Pointer Type as Container Pointer Type
    unsigned char *source = (unsigned char *)item;
    unsigned char *dest = Queue_GetItem(queue, index);
    //copy byte by byte
    memcpy(dest, source, queue->TypeSize);
}

//---------------------------Public Method---------------------------------
void Queue_Initialize(struct Queue *queue, unsigned char* container, int maxAmount, int typeSize)
{
    queue->Container = container;    
    queue->MaxAmount = maxAmount;
    queue->TypeSize = typeSize;
    queue->Count = 0;
    queue->StartIndex = 0;
    queue->EndIndex = 0;    
}

void Queue_Enqueue(struct Queue *queue, void *item)
{
    //Overflow Array Index => reset
    if(queue->StartIndex >= queue->MaxAmount)
    {
        queue->StartIndex = 0;
    }
    if(queue->EndIndex >= queue->MaxAmount)
    {
        queue->EndIndex = 0;
    }

    //Overflow Count => increase EndIndex
    if(queue->EndIndex == queue->StartIndex && queue->Count > 0)
    {
        queue->EndIndex++;
    }

    //Set item value, Update Index
    Queue_SetItem(queue, queue->StartIndex, item);
    queue->StartIndex++;

    //Update Count
    if(queue->Count < queue->MaxAmount)
    {
        queue->Count++;
    }
}

void* Queue_Dequeue(struct Queue *queue)
{
    void* item = NULL;
    //Overflow Max Amount
    if(queue->EndIndex >= queue->MaxAmount)
    {
        queue->EndIndex = 0;
    }

    //Update Counter
    if(queue->Count > 0)
    {
        queue->Count--;
    }

    //Get item
    item = Queue_GetItem(queue, queue->EndIndex);
    
    //Update EndIndex
    queue->EndIndex++;    
    return item;
}

void* Queue_Peek(struct Queue *queue)
{
    //Overflow Max Amount
    if(queue->EndIndex > queue->MaxAmount)
    {
        queue->EndIndex = 0;
    }
    return Queue_GetItem(queue, queue->EndIndex);
}

void QueueInfo_Initialize(struct QueueInfo *info)
{
    info->IsValid = 0;
    info->Count = 0;
    info->StartIndex = 0;
    info->EndIndex = 0;
}

void QueueInfo_SetInfo(struct QueueInfo *info, struct Queue *queue)
{
    info->IsValid = 1;
    info->Count = queue->Count;
    info->StartIndex = queue->StartIndex;
    info->EndIndex = queue->EndIndex;
}

void QueueInfo_SetQueue(struct QueueInfo *info, struct Queue *queue)
{
    queue->Count = info->Count;
    queue->StartIndex = info->StartIndex;
    queue->EndIndex = info->EndIndex;
}