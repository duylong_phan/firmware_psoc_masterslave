#include <stdio.h>
#include "SubFunctions.h"
#include "HeaderPackage.h"   

#ifndef ESP8266_h
#define ESP8266_h   
    #define ESP8266_Mode_Station            1
    #define ESP8266_Mode_AP                 2
    #define ESP8266_Mode_AP_Station         3
    
    #define ESP8266_Enc_OPEN                0
    #define ESP8266_Enc_WEP                 1
    #define ESP8266_Enc_WPA_PSK             2
    #define ESP8266_Enc_WPA2_PSK            3
    #define ESP8266_Enc_WPA_WPA2_PSK        4
    
    #define ESP8266_DHCP_SoftAP             0
    #define ESP8266_DHCP_Station            1
    #define ESP8266_DHCP_SoftAP_Station     2
    
    #define ESP8266_Echo_Enable             1
    #define ESP8266_Echo_Disable            0
    
    #define ESP8266_Enable                  0
    #define ESP8266_Disable                 1
    
    #define ESP8266_Mux_Single              0
    #define ESP8266_Mux_Multi               1
    
    #define ESP8266_Server_Delete           0
    #define ESP8266_Server_Create           1
        
    #define ESP8266_Status_Non              0
    #define ESP8266_Status_Ok               1
    #define ESP8266_Status_Error            2
    #define ESP8266_Status_HasRequest       3
    #define ESP8266_Status_WaitResponse     4
    
    struct ESP8266Expression
    {
        char *Text;
        unsigned char Length;
        unsigned char Count;
        unsigned char Result;
    };
    
    struct ESP8266Handler
    {
        struct HeaderPackage InputPackage;
        struct ESP8266Expression *Expressions;
        char RequestInfo[32];
        unsigned char InputPackageCount;
        unsigned char ExpressionCount;        
        unsigned char RequestIndex;
        unsigned char WaitRequestInfo;
        unsigned short Pipe;
        unsigned short Length;
    };
    
    //-------------------Software API-----------------------------
    void ESP8266Expression_Initialize(struct ESP8266Expression *expression, char *text, unsigned char result);
    void ESP8266Handler_Initialize(struct ESP8266Handler *handler, unsigned char *dataContainer, unsigned char inputPackageCount);
    unsigned char ESP8266_Parse(struct ESP8266Handler *handler, unsigned char value);
    void ESP8266Handler_GetRequestInfo(struct ESP8266Handler *handler, unsigned char value);
    void ESP8266Handler_ParseRequestInfo(struct ESP8266Handler *handler);
    
    //--------------------Hardware API-----------------------------
    void ESP8266_CheckInput(struct ESP8266Handler *handler);
    void ESP8266_WriteOut(const char *command);    
    void ESP8266_WriteOutBuffer(const unsigned char *buffer, int length);
    
    void ESP8266_Reset();
    void ESP8266_CheckDevice();
    void ESP8266_SetEcho(unsigned char value);
    void ESP8266_SetMode(unsigned char mode);
    void ESP8266_SetIpAddress(const char *address);
    void ESP8266_SetMacAddress(const char *address);
    void ESP8266_SetMuxConnection(unsigned char value);
    void ESP8266_TurnWlanOn(const char *ssid, const char *pass, unsigned char channel, unsigned char encode);
    void ESP8266_StartWebServer(unsigned short port);
    void ESP8266_StopWebServer(unsigned short port);
    
    void ESP8266_WriteResponse(struct ESP8266Handler *handler, unsigned char *buffer, int length);
#endif