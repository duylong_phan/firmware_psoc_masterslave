#include "Config.h"   
#include "Definitions.h"
#include "Queue.h"

#ifndef Header_Package_h
#define Header_Package_h
    #define HeaderPackage_Indicator             '#'
    #define HeaderPackage_MaxPayload            64
    
    struct HeaderPackage
    {
        struct Queue Queue;
        unsigned char HasHeader, HasLength;
        unsigned char HeaderCount, Length, Type;
        unsigned char PayLoad[HeaderPackage_MaxPayload];
    };
    
	/// <summary>
    /// Initialize given HeaderPackage instance
    /// </summary>
    /// <param name="package">given instance</param>
    /// <param name="container">data container</param>
    void HeaderPackage_Intialize(struct HeaderPackage *package, unsigned char* container);
	
	/// <summary>
    /// Set UsbHandler instance, only for PSOC5 with usb support
    /// </summary>
    /// <param name="usbHandler">given instance</param>
    void HeaderPackage_SetUsbHandler(struct UsbHandler *usbHandler);
	
	/// <summary>
    /// Write given data to Computer
    /// </summary>
    /// <param name="data">given data container</param>
    /// <param name="length">data length</param>
    void HeaderPackage_WritePackage(const unsigned char *data, int length);
	
	/// <summary>
    /// Write all data in buffer to Computer, only for PSOC5 with usb support
    /// </summary>
    void HeaderPackage_Flush();
	
	/// <summary>
    /// Check if given Input Buffer contains Computer Command
    /// </summary>
    /// <param name="package">given data container</param>    
	unsigned char HeaderPackage_Parse(struct HeaderPackage *package); 
#endif