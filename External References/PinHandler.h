#ifndef PinHandler_h
#define PinHandler_h    
    struct PinHandler
    {
        unsigned char ID;        
        unsigned char Command;
        unsigned short Count;
        unsigned short Duration;
        void (*Write)(unsigned char);
        unsigned char (*Read)();
    };
    
    void PinHandler_Initialize(struct PinHandler *handler, unsigned char id, void (*write)(unsigned char), unsigned char (*read)());    
#endif