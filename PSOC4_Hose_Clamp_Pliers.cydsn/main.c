#include "Config.h"
#include "../External References/Definitions.h"
#include "../External References/Queue.h"
#include "../External References/StreamBuffer.h"
#include "../External References/SubFunctions.h"
#include "../External References/RFM7X.h"

//-------------------------Const Definitions------------------------
#define Size_SampleBuffer               20
#define Size_SamplePaar                 5
#define Size_ValueContainer             64
#define Index_Check_Pos                 -3 

#define Beep_Non                        0
#define Beep_Start                      1
#define Beep_Closed                     2
#define Beep_Done                       3
#define Beep_Received                   4

#define Method_Press_PressedDuration    0x01
#define Method_Press_TriggerCurve       0x02
#define Enabled_Beep_On_Start           0x01
#define Enabled_Beep_On_Closed          0x02
#define Enabled_Beep_On_Done            0x04
#define Enabled_Beep_On_Received        0x08

#define Setting_Press_ID                0
#define Setting_Press_Method            1
#define Setting_Press_Frequency         2
#define Setting_Press_WindowSize        3
#define Setting_Press_PointAmount       4
#define Setting_Press_Mag_Beta          5
#define Setting_Press_Force_Beta        6
#define Setting_Press_Min_MagD          7
#define Setting_Press_Max_MagD          9
#define Setting_Press_Min_Force         11
#define Setting_Press_Max_Force         13
#define Setting_Press_Release_Force     15
#define Setting_Press_Enable_Beep       17
#define Setting_Press_Short_Beep        18
#define Setting_Press_Long_Beep         19
#define Setting_Press_Beep_On_Start     20
#define Setting_Press_Beep_On_Closed    21
#define Setting_Press_Beep_On_Done      22
#define Setting_Press_Beep_On_Received  23
#define Setting_Press_DoneAmount        28
#define Setting_Press_RF_Channel        29
#define Setting_Press_RF_Rx_Address     30
#define Setting_Press_RF_Tx_Address     35
#define Setting_Press_AbsForceDeltaSum  40
//--------------------------Struct, Union----------------------
union SampleBuffer
{
    unsigned char Array[Size_SampleBuffer];
    short Number[Size_SamplePaar*2];    
};

struct ToolHandler
{
    //for Debug Support
    union SampleBuffer Sample;
    short Amount;    
    
    //status Flags
    unsigned char CanWaitCommand;
    unsigned char CanDebug;
    unsigned char IsReleased; 
    
    //Transmit Parameter
    unsigned char PackageType;
    unsigned char LastPackageType;
    unsigned char PressAmount;
    unsigned char CurrentAmount;
    unsigned char PackageIndex;
    
    //Beep Operation
    short BeepCount;
    short BeepType;
    short BeepAmount;
    
    //Others
    short LastMag;
    short LastForce;
    int TickCount;
};

//--------------------------Buffer--------------------------
unsigned char __stateContainer[Size_StateContainer];
unsigned char __magDContainer[Size_ValueContainer*2];             //important => sizeof(short)
unsigned char __forceContainer[Size_ValueContainer*2];            //important => sizeof(short)


//--------------------------Setting----------------------------
//more information about the setting, read attached Document
static const unsigned char SettingBuffer[Size_SettingBuffer] = 
{
    0x01, 0x02, 0x50, 0x32, 0x1E,
    0x28, 0x5A, 0xF4, 0x01, 0xB8,
    0x0B, 0xCA, 0x08, 0xB8, 0x0B,
    0xDC, 0x05, 0x0F, 0x32, 0xC8, 
    0x02, 0x01, 0x03, 0x01, 0x00,
    0x00, 0x00, 0x00, 0x02, 
    0x14, 0x34, 0x43, 0x10, 0x10, 
    0x01, 0x34, 0x43, 0x10, 0x10, 
    0x01, 0x14, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00
};
const unsigned char *Setting_ID = &SettingBuffer[Setting_Press_ID];
const unsigned char *Setting_Method = &SettingBuffer[Setting_Press_Method];
const unsigned char *Setting_Frequency = &SettingBuffer[Setting_Press_Frequency];
const unsigned char *Setting_WindowSize = &SettingBuffer[Setting_Press_WindowSize];
const unsigned char *Setting_PointAmount = &SettingBuffer[Setting_Press_PointAmount];
const unsigned char *Setting_Mag_Beta = &SettingBuffer[Setting_Press_Mag_Beta];
const unsigned char *Setting_Force_Beta = &SettingBuffer[Setting_Press_Force_Beta];
const unsigned char *Setting_Enable_Beep = &SettingBuffer[Setting_Press_Enable_Beep];
const unsigned char *Setting_Short_Beep = &SettingBuffer[Setting_Press_Short_Beep];
const unsigned char *Setting_Long_Beep = &SettingBuffer[Setting_Press_Long_Beep];
const unsigned char *Setting_Beep_On_Start = &SettingBuffer[Setting_Press_Beep_On_Start];
const unsigned char *Setting_Beep_On_Closed = &SettingBuffer[Setting_Press_Beep_On_Closed];
const unsigned char *Setting_Beep_On_Done = &SettingBuffer[Setting_Press_Beep_On_Done];
const unsigned char *Setting_Beep_On_Received = &SettingBuffer[Setting_Press_Beep_On_Received];
const unsigned char *Setting_DoneAmount = &SettingBuffer[Setting_Press_DoneAmount];
const unsigned char *Setting_RF_Channel = &SettingBuffer[Setting_Press_RF_Channel];
const unsigned char *Setting_RF_Rx_Address = &SettingBuffer[Setting_Press_RF_Rx_Address];
const unsigned char *Setting_RF_Tx_Address = &SettingBuffer[Setting_Press_RF_Tx_Address];
const unsigned char *Setting_AbsForceDeltaSum = &SettingBuffer[Setting_Press_AbsForceDeltaSum];

//---------------------------Global Variable--------------------
struct Queue stateQueue;
struct RfHandler rfHandler;
struct StreamBuffer forceBuffer, magDBuffer;
struct ToolHandler toolHandler;

//-------------------Scaled Value from SettingBuffer--------------------
float Scaled_Mag_Beta, Scaled_Force_Beta;
short Scaled_Min_MagD, Scaled_Max_MagD;
short Scaled_Min_Force , Scaled_Max_Force , Scaled_Release_Force;
short Scaled_Frequency;

//---------------------------Function Prototypes---------------------------
//Input Process
void GetDeviceStatus(unsigned int pcAction, unsigned int status);
void ToggleDebugStatus();
void GetSettingPackage(int index);
void SetSettingPackage(int index);

//Status
void On_Beep_WaitToStop();
void On_Beep_WaitToStart();

//Calculate
int CompareShort(void *x, void *y);
short GetAbsShort(short value);
short UseLowPass(short input, short lastOut, float beta);
unsigned char HasTriggerCurve(int last, int current, int next);

//Detect method
unsigned char Check_PressDuration();
unsigned char Check_TriggerCurve();

//----------------------------Main Method-----------------------
int main()
{  
    Initialize_HW();
    Initialize_SW();
    Initialize_Parameters();
    for(;;)
    {
        Single_Operation();    
    }
}

CY_ISR(Triggered_RF_Inter)
{
    On_RF_Inter();
}

CY_ISR(Triggered_Timer_Inter)
{
    On_Timer_Inter();
}

//---------------------------Initialize HW, SW-------------------
//Initialize Hardware Components and APIs
void Initialize_HW()
{
    //Important => always call this
    CyGlobalIntEnable; 
    
    //ADC: enable background sample for analog signal
    ADC_Start();
    ADC_StartConvert();
    
    //SPI: enable RF, started with Rx mode
    SPIM_Start();
    RFM7X_Start();
    RFM7X_SetChannel(*Setting_RF_Channel);
    RFM7X_SetAddress_Tx((unsigned char *)Setting_RF_Rx_Address);
    RFM7X_SetAddress_Rx((unsigned char *)Setting_RF_Tx_Address);  
    
    //Enable RF Interrrupt
    RF_Inter_StartEx(Triggered_RF_Inter);
    
    //Enable Timer Interrrupt
    Timer_Inter_StartEx(Triggered_Timer_Inter);    
}

//Initialize Software components
void Initialize_SW()
{
    //RfHandler: variable to manage RF communication
    //Important => muss be matched with Initialize_HW()
    rfHandler.Pipe = 0;
    rfHandler.Length = 0;
    rfHandler.LostCount = 0;
    rfHandler.Mode = RF_Receiver;
    rfHandler.CheckInput = 0;
    rfHandler.WaitOutput = 0;
    Sub_ResetArray_Unsigned(rfHandler.TransmitPackage, Size_RF_Package);
    Sub_ResetArray_Unsigned(rfHandler.ReceivePackage, Size_RF_Package);
    
    //ToolHandler: variable to manage status of tool
    toolHandler.CanWaitCommand = 1;
    toolHandler.CanDebug = 0;
    toolHandler.IsReleased = 0;
    toolHandler.BeepType = Beep_Non;
    toolHandler.BeepCount = 0;
    toolHandler.BeepAmount = 0;
    toolHandler.PackageType = Type_Press_Non;
    toolHandler.LastPackageType = Type_Press_Non;
    toolHandler.PressAmount = 0;    
    toolHandler.Amount = -1;
    toolHandler.PackageIndex = 0;
    toolHandler.LastForce = 0;
    toolHandler.LastMag = 0;
    toolHandler.TickCount = 0;         
    
    //Scaled value from SettingBuffer => more info in Document
    Scaled_Mag_Beta = *Setting_Mag_Beta / 100.0;
    Scaled_Force_Beta = *Setting_Force_Beta / 100.0;
    Scaled_Frequency = 1000 / *Setting_Frequency;
    Scaled_Min_MagD = *(const short*)&SettingBuffer[Setting_Press_Min_MagD];
    Scaled_Max_MagD = *(const short*)&SettingBuffer[Setting_Press_Max_MagD];
    Scaled_Min_Force = *(const short*)&SettingBuffer[Setting_Press_Min_Force];
    Scaled_Max_Force = *(const short*)&SettingBuffer[Setting_Press_Max_Force];
    Scaled_Release_Force = *(const short*)&SettingBuffer[Setting_Press_Release_Force];
    
    //StateQueue: uC operation state Queue
    Queue_Initialize(&stateQueue, __stateContainer, Size_StateContainer, sizeof(unsigned char));    
    
    //StreamBuffer: support store and calculate Window Samples
    StreamBuffer_Initialize(&magDBuffer, __magDContainer, *Setting_WindowSize, sizeof(short), CompareShort);    
    StreamBuffer_Initialize(&forceBuffer, __forceContainer, *Setting_WindowSize, sizeof(short), CompareShort);
}

//Intialize Parameter: set, reset HW and SW Components
void Initialize_Parameters()
{
    //Start Timer
    Timer_PWM_Start();
    
    //Notify device in ready
    LED_1_Write(1);
    if((*Setting_Enable_Beep & Enabled_Beep_On_Start) != 0)
    {
        toolHandler.BeepType = Beep_Start;
        toolHandler.BeepAmount = *Setting_Beep_On_Start;
    }
}

//Life circle of the Microcontroller
void Single_Operation()
{
    unsigned char state = (stateQueue.Count > 0) ? *(unsigned char*)Queue_Dequeue(&stateQueue) : State_Wait_Input;
    switch(state)
    {
        case State_RF_Interrupt:        Do_RF_Interrupt();      break;
        case State_RF_Receive:          Do_RF_Receive();        break;
        case State_RF_Transmit:         Do_RF_Transmit();       break;
        case State_Process_Input:       Do_Process_Input();     break;
        case State_Generate_Output:     Do_Generate_Output();   break;
        case State_Task:                Do_Task();              break;
        case State_Wait_Input:
        default:                        Do_Wait_Input();        break;
    }
}

//--------------------Interrupt Method------------------
void On_RF_Inter()
{
    //Request RF Interrupt
    unsigned char nextState = State_RF_Interrupt;
    Queue_Enqueue(&stateQueue, &nextState);
}

void On_Timer_Inter()
{
    unsigned char nextState = State_Wait_Input;
    
    //Time tracker
    toolHandler.TickCount++;  
    if(toolHandler.TickCount > TickCount_Minute)
    {
        toolHandler.TickCount = 0;
        //Reset Flag => no Rx on RF
        if(toolHandler.CanWaitCommand)
        {
            toolHandler.CanWaitCommand = 0;
        }
    }   
    
    //check if transmission is enabled
    if(rfHandler.WaitOutput > 0)
    {
        rfHandler.WaitOutput--;
        if(rfHandler.WaitOutput == 0)
        {
            nextState = State_RF_Transmit;
            Queue_Enqueue(&stateQueue, &nextState);
        }
    }
    
    //Request Do task
    if((toolHandler.TickCount % Scaled_Frequency) == 0)
    {
        nextState = State_Task;
        Queue_Enqueue(&stateQueue, &nextState);
    }
    
    //Request Beep
    if(toolHandler.BeepType != Beep_Non)
    {
        if(toolHandler.BeepCount > 0)
        {
            On_Beep_WaitToStop();
        }
        else
        {
            On_Beep_WaitToStart();
        }
    }
}

//---------------------------Operation Method--------------------
//On RF Interrupt, or requested
void Do_RF_Interrupt()
{    
    unsigned char hasIn, hasOut, hasLost;    
    
    //Wait chip to update and read Interrupt status
    Sub_DelayUs(50);
    RFM7X_GetInterrupt(&hasIn, &hasOut, &hasLost);        
    
    //When transmision completed
    if(hasOut)
    {     
        LED_2_Write(0); 
        //On Pressed Package => Reset Amount
        if((toolHandler.LastPackageType & Type_Press_Pressed) != 0)
        {
            toolHandler.PressAmount = 0;
        }
    } 
    
    //Only when RF is in Transmitter mode
    if(rfHandler.Mode == RF_Transmitter)
    {
        //Wait Command from Master
        if(toolHandler.CanWaitCommand)
        {
            rfHandler.Mode = RF_Receiver;
            RFM7X_Mode_Rx();
        }
        //Turn off to save Power
        else
        {
            rfHandler.Mode = RF_Off;
            RFM7X_PowerDown();
        }
    }
}

//Read Package from Master
void Do_RF_Receive()
{
    unsigned char nextState = State_Process_Input;
    unsigned char hasData = RFM7X_Receive(rfHandler.ReceivePackage, &rfHandler.Length, &rfHandler.Pipe);        
    //Request next operation state when data is available
    if(hasData)
    {        
        Queue_Enqueue(&stateQueue, &nextState);
        LED_2_Write(0);
    }
}

//Transmit package to Master
void Do_RF_Transmit()
{
    //wait until WaitOutput reaches 0
    if(rfHandler.WaitOutput > 0)
    {
        return;
    }
    
    //To Tx, if current mode is different
    if(rfHandler.Mode != RF_Transmitter)
    {
        rfHandler.Mode = RF_Transmitter;        
        RFM7X_Mode_Tx();
    }
    //Clear Interrupt
    else
    {
        RFM7X_ClearInterrupt();
    }   
    //Before transmitting package, always clear Interrupt, explicit or indirect in RFM7X_Mode_Tx
    //to allow transmision, ignore current State of RF Module
    RFM7X_Transmit(rfHandler.TransmitPackage, Size_RF_Package);
    LED_2_Write(1);
    
    //later usage in Do_RF_Interrupt
    toolHandler.LastPackageType = rfHandler.TransmitPackage[Index_Type];
}

//Process Input Package from Master
void Do_Process_Input()
{
    unsigned char hasHandler = 1;
    
    //Check TaskID
    if(rfHandler.ReceivePackage[Index_TaskID] != TaskID_Press)
    {
        return;
    }
    
    //Comsume Package
    switch(rfHandler.ReceivePackage[Index_Type])
    {
        case Type_Press_DeviceStatus:       GetDeviceStatus(1 << PC_Action_Bit_AppStatus, 0);                                                          break;
        case Type_Press_Debug:              ToggleDebugStatus();                                                        break;
        case Type_Press_ReadSetting:        GetSettingPackage(rfHandler.ReceivePackage[Pos_Press_SettingIndex]);        break;
        case Type_Press_WriteSetting:       SetSettingPackage(rfHandler.ReceivePackage[Pos_Press_SettingIndex]);        break;
        case Type_Press_Non:
        default:                            hasHandler = 0;                                                             break;        
    }
    
    //Notify User
    if(hasHandler && (*Setting_Enable_Beep & Enabled_Beep_On_Received) != 0)
    {
        toolHandler.BeepType = Beep_Received;
        toolHandler.BeepAmount = *Setting_Beep_On_Received;
    }
}

//Generate Package in order to transmit to Master
void Do_Generate_Output()
{
    unsigned char nextState = State_RF_Transmit;
    
    //Assign parameter
    rfHandler.TransmitPackage[Index_TaskID] = TaskID_Press;
    rfHandler.TransmitPackage[Index_Type] = toolHandler.PackageType;
    rfHandler.TransmitPackage[Pos_Press_ID] = *Setting_ID;
    
    //On Pressed Package
    if((toolHandler.PackageType & Type_Press_Pressed) != 0)
    {
        rfHandler.TransmitPackage[Pos_Press_Amount] = toolHandler.PressAmount;       
        
        //Important => check press amount in master device
        rfHandler.TransmitPackage[Pos_Press_PackageIndex] = toolHandler.PackageIndex;
        //for next Type_Press_Pressed package
        toolHandler.PackageIndex++;
        
        //only in normal operation
        //avoid transmission during beeper time => Beeper may take too much power
        if(toolHandler.CanDebug == 0)
        {
            rfHandler.WaitOutput = 500;
        }      
    }
    
    //On Debug Package
    if((toolHandler.PackageType & Type_Press_Debug) != 0)
    {
        memcpy(&rfHandler.TransmitPackage[Pos_Press_Sample], toolHandler.Sample.Array, Size_SampleBuffer);
    }
    
    //Request next operation state
    Queue_Enqueue(&stateQueue, &nextState);
}

//Do specific Task
void Do_Task()
{
    unsigned char nextState = State_Wait_Input;
    unsigned char packageType = Type_Press_Non;    
    unsigned char okCount = 0, checkCount = 0;
    short mag, magD, force;
    short tmpValue, tmpOffset;
    
    //-----------------Get ADC Value-----------------
    //in Number
    force = ADC_GetResult16(0);
    mag = ADC_GetResult16(1);
    //in mV
    force = ADC_CountsTo_mVolts(0, force);
    mag = ADC_CountsTo_mVolts(1, mag);
    
    //first sample => capture, return
    if(toolHandler.Amount < 0)
    {
        toolHandler.LastForce = force;
        toolHandler.LastMag = mag;        
        toolHandler.Amount++;
        return;
    }
    
    //-----------------filter input value--------
    force = UseLowPass(force, toolHandler.LastForce, Scaled_Force_Beta);
    mag = UseLowPass(mag, toolHandler.LastMag, Scaled_Mag_Beta);
    magD = (mag - toolHandler.LastMag) * (*Setting_Frequency);
    magD = GetAbsShort(magD);
    
    //-----------------store-----------------------
    toolHandler.LastForce = force;
    toolHandler.LastMag = mag;
    StreamBuffer_Add(&forceBuffer, &force);
    StreamBuffer_Add(&magDBuffer, &magD);
    
    //-----------------Release status-----------------
    if(toolHandler.IsReleased == 0 && force < Scaled_Release_Force)
    {
        toolHandler.IsReleased = 1; 
    }  
    
    //-----------------Check press action-----------------
    //no Beep, Tool is released
    if(toolHandler.BeepCount == 0 && toolHandler.IsReleased)
    {
        tmpOffset = StreamBuffer_GetOffset(magDBuffer.MaxIndex, magDBuffer.StartIndex);
        tmpValue = *(short*)StreamBuffer_GetItem(&magDBuffer, magDBuffer.MaxIndex);
        //at Check_Position, MagD Range
        if(tmpOffset == Index_Check_Pos && tmpValue > Scaled_Min_MagD && tmpValue < Scaled_Max_MagD)
        {
            tmpValue = *(short*)StreamBuffer_GetItem(&forceBuffer, forceBuffer.MaxIndex);
            //Force in Range => tool is closing => Apply Detect Method
            if(tmpValue > Scaled_Min_Force)
            {
                if((*Setting_Method & Method_Press_PressedDuration) != 0)
                {
                    checkCount++;
                    if(Check_PressDuration() > 0)
                    {
                        okCount++;
                    }
                }
                if((*Setting_Method & Method_Press_TriggerCurve) != 0)
                {
                    checkCount++;
                    if(Check_TriggerCurve() > 0)
                    {
                        okCount++;
                    }
                }
            }
                
        }
    }    
    //Press Action was detected
    if(okCount != 0 && okCount == checkCount)
    {
        //set, reset
        toolHandler.IsReleased = 0;
        toolHandler.PressAmount++;
        toolHandler.CurrentAmount++;
        packageType |= Type_Press_Pressed;
        
        //notify user        
        if((*Setting_Enable_Beep & Enabled_Beep_On_Closed) != 0)
        {
            toolHandler.BeepType = Beep_Closed;
            toolHandler.BeepAmount = *Setting_Beep_On_Closed;
        }      
        //Job is done
        if(toolHandler.CurrentAmount >= *Setting_DoneAmount && (*Setting_Enable_Beep & Enabled_Beep_On_Done) != 0)
        {
            toolHandler.CurrentAmount = 0;
            toolHandler.BeepType = Beep_Done;
            toolHandler.BeepAmount = *Setting_Beep_On_Done;
        }
    } 
    
    //-----------------Debug is enabled-----------------
    if(toolHandler.CanDebug)
    {
        //better Debug view   
        if(magD < Scaled_Min_MagD)
        {
            magD = 0;
        }  
        
        //only magD, and Force
        toolHandler.Sample.Number[toolHandler.Amount*2] = magD;
        toolHandler.Sample.Number[toolHandler.Amount*2 + 1] = force;
        toolHandler.Amount++; 
        
        //reset, request output
        if(toolHandler.Amount >= Size_SamplePaar)
        {   
            toolHandler.Amount = 0;
            packageType |= Type_Press_Debug;                 
        }
    }
    
    //check if Transmision is available
    if(packageType != Type_Press_Non)
    {
        //Update current Package Type
        toolHandler.PackageType = packageType;
        
        //Request next operation state
        nextState = State_Generate_Output;  
        Queue_Enqueue(&stateQueue, &nextState);
    }
}

//Do nothing
void Do_Wait_Input()
{
    unsigned char nextState = State_Wait_Input;
    
    //WaitCommand Duration is passed, RF in Receive Mode => turn off to save power
    if(toolHandler.CanWaitCommand == 0 && rfHandler.Mode == RF_Receiver)
    {
        rfHandler.Mode = RF_Off;
        RFM7X_PowerDown();
    }
    
    //When Input from Master
    //return when available
    if(rfHandler.Mode == RF_Receiver && RFM7X_HasData())
    {
        nextState = State_RF_Receive;
        Queue_Enqueue(&stateQueue, &nextState);
        LED_2_Write(1);
        return;
    }   
    
    //Do thing
    //Processor will do background works
    //or Processor will be turned off to save energy
    Sub_DelayMs(1);
}

//----------------------------Shared Methods--------------------------------

void GetDeviceStatus(unsigned int pcAction, unsigned int status)
{   
    //Device is available
    status |= 1 << Status_Bit_Press_Available;
    
    //assign Transmision package
    rfHandler.TransmitPackage[Index_TaskID] = TaskID_Press;
    rfHandler.TransmitPackage[Index_Type] = Type_Press_DeviceStatus;
    rfHandler.TransmitPackage[Pos_Press_ID] = *Setting_ID;
    memcpy(&rfHandler.TransmitPackage[Pos_Press_PcAction], &pcAction, sizeof(unsigned int));
    memcpy(&rfHandler.TransmitPackage[Pos_Press_Status], &status, sizeof(unsigned int));
    
    //Important => Wait Master to Rx
    //State_RF_Transmit is requested, when WaitOutput reaches 0
    rfHandler.WaitOutput = RF_SwitchWait;
}

void ToggleDebugStatus()
{
    unsigned statusBit = 0;
    if(toolHandler.CanDebug)
    {
        toolHandler.CanDebug = 0;
        statusBit = Status_Bit_Press_SampleOff;
    }
    else
    {
        toolHandler.CanDebug = 1;
        statusBit = Status_Bit_Press_SampleOn;
    }
    GetDeviceStatus( 1 << PC_Action_Bit_AppStatus, 1 << statusBit);
}

void GetSettingPackage(int index)
{
    //assign Transmision package
    rfHandler.TransmitPackage[Index_TaskID] = TaskID_Press;
    rfHandler.TransmitPackage[Index_Type] = Type_Press_ReadSetting;
    rfHandler.TransmitPackage[Pos_Press_SettingIndex] = index;
    memcpy(&rfHandler.TransmitPackage[Pos_Press_SettingContent], 
           SettingBuffer + index*Size_Press_SettingPackage, 
           Size_Press_SettingPackage);  
    
    //Important => Wait Master to Rx
    //State_RF_Transmit is requested, when WaitOutput reaches 0
    rfHandler.WaitOutput = RF_SwitchWait;
}

void SetSettingPackage(int index)
{
    //Write into Flash Memory
    EEPROM_Start();
    EEPROM_Write(&rfHandler.ReceivePackage[Pos_Press_SettingContent], 
                 SettingBuffer + index*Size_Press_SettingPackage, 
                 Size_Press_SettingPackage);
    EEPROM_Stop();
    
    //Last Index of setting => Device reset
    if(index == 1)
    {
        CySoftwareReset();
    }
}

//compare 2 short number
int CompareShort(void *x, void *y)
{
    int result = 0;
    short *a = (short*)x;
    short *b = (short*)y;

    if(*a > *b)
    {
        result = 1;
    }
    else if(*a < *b)
    {
        result = -1;
    }

    return result;
}

//get absolute value of a short number
short GetAbsShort(short value)
{
    return (value < 0) ? -value : value;
}

//apply low pass filter 
short UseLowPass(short input, short lastOut, float beta)
{
    float output = beta*input + (1 - beta)*lastOut;  
    return (short)output;
}

//Check if TriggerCurve is at current position
unsigned char HasTriggerCurve(int last, int current, int next)
{
    unsigned char hasTrigger = 0;
    short delta1, delta2;
    
    delta1 = *(short*)StreamBuffer_GetItem(&forceBuffer, current) - *(short*)StreamBuffer_GetItem(&forceBuffer, last);
    delta2 = *(short*)StreamBuffer_GetItem(&forceBuffer, next) - *(short*)StreamBuffer_GetItem(&forceBuffer, current);
    
    if(delta1 < 0 && delta2 > 0)
    {
        delta1 = GetAbsShort(delta1) + GetAbsShort(delta2);
        if(delta1 > *Setting_AbsForceDeltaSum)
        {
            hasTrigger = 1;
        }
    }    
    return hasTrigger;
}

//apply PressDuration method
unsigned char Check_PressDuration()
{
    unsigned char hasPressAction = 0;
    short i, index, tmpValue;
    short count = 0;
    
    //-----------------find first value in Force Range-----------------
    //index => start check position
    index = StreamBuffer_NextIndex(&magDBuffer, magDBuffer.MaxIndex);
    //i     => prevent forever loop, or invalid Memory access
    i = *Setting_WindowSize - GetAbsShort(Index_Check_Pos);    
    while(i >= 0)
    {
        tmpValue= *(short*)StreamBuffer_GetItem(&magDBuffer, index);
        if(tmpValue > Scaled_Min_Force)
        {
            break;
        }
        index = StreamBuffer_NextIndex(&magDBuffer, index);
        i--;
    }   
    //Count value in Force Range, until reach StartIndex
    while(index != forceBuffer.StartIndex)
    {
        tmpValue = *(short*)StreamBuffer_GetItem(&forceBuffer, index);        
        if(tmpValue > Scaled_Min_Force)
        {
            count++;
        }            
        index = StreamBuffer_NextIndex(&forceBuffer, index);
    }
    
    hasPressAction = (count >= *Setting_PointAmount) ? 1 : 0;    
    return hasPressAction;
}


//apply TriggerCurve method
unsigned char Check_TriggerCurve()
{
    unsigned char hasPressAction = 0;
    unsigned char hasNoise = 0;
    short tmpValue = 0;
    int i = 0;
    int index[7];
    
    //Task Point in range [-3;3] vs MaxIndex of MagD
    //due to Low Pass Filter => Character of Curve changed => delay or shift
    index[3] = magDBuffer.MaxIndex;
    index[4] = StreamBuffer_NextIndex(&magDBuffer, index[3]);
    index[5] = StreamBuffer_NextIndex(&magDBuffer, index[4]);
    index[6] = StreamBuffer_NextIndex(&magDBuffer, index[5]);
    index[2] = StreamBuffer_LastIndex(&magDBuffer, index[3]);
    index[1] = StreamBuffer_LastIndex(&magDBuffer, index[2]);
    index[0] = StreamBuffer_LastIndex(&magDBuffer, index[1]);
    
    //Check Noise
    for(i = 0; i < 5; i++)
    {
        tmpValue = *(short*)StreamBuffer_GetItem(&forceBuffer, index[i]);
        if(tmpValue < Scaled_Min_Force)
        {
            hasNoise = 1;
            break;
        }
    }    
    //when noise is not available
    if(hasNoise == 0)
    {
        for(i = 0; i < 5; i++)
        {
            if(HasTriggerCurve(index[i], index[i+1], index[i+2]))
            {
                hasPressAction = 1;
                break;
            }
        }     
    }    
    return hasPressAction;
}

//When beep sounds is requested to stop
void On_Beep_WaitToStop()
{
    short maxBeep = 0;
    toolHandler.BeepCount++;
    switch(toolHandler.BeepType)
    {
        case Beep_Start:            
        case Beep_Received:         maxBeep = *Setting_Short_Beep;      break;
        case Beep_Closed:
        case Beep_Done:
        default:                    maxBeep = *Setting_Long_Beep;       break;
    }
    
    if(toolHandler.BeepCount >= maxBeep)
    {
        //set, reset        
        toolHandler.BeepAmount--;
        if(toolHandler.BeepAmount <= 0)
        {
            toolHandler.BeepType = Beep_Non;
            //start immediately on next Beep request
            toolHandler.BeepCount = 0;
        }
        //avoid to start beep immediately
        else
        {
            toolHandler.BeepCount = -1;
        }
        
        //Stop HW PWM
        Beep_PWM_Stop();
    }
}

//When beep sounds is requested to start
void On_Beep_WaitToStart()
{
    unsigned char canStart = 0;
    short maxBeep = 0;      
    
    //Start immediately after requesting
    if(toolHandler.BeepCount == 0)
    {
        canStart = 1;
    }
    //Wait to start
    else
    {
        toolHandler.BeepCount--;    
        switch(toolHandler.BeepType)
        {
            case Beep_Start:
            case Beep_Received:         maxBeep = *Setting_Short_Beep;      break;
            case Beep_Closed:
            case Beep_Done:
            default:                    maxBeep = *Setting_Long_Beep;       break;
        }
        
        if(GetAbsShort(toolHandler.BeepCount) >= maxBeep)
        {
            canStart = 1;
        }
    }
    if(canStart)
    {
        toolHandler.BeepCount = 1;
        Beep_PWM_Start();
    }
}