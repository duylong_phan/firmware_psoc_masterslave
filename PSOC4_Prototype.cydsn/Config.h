#ifndef Config_h
#define Config_h  
    
    //--------------- microcontroller--------------------
    #define PSOC4_Mode                      1
    //#define PSOC5_Mode                      1
    //#define Arduino_Mode                    1

    #if defined(PSOC4_Mode) || defined(PSOC5_Mode)
    #include <project.h>
    #endif

    #ifdef Arduino_Mode
        #include <Arduino.h>
    #endif
    
	//------------------Components-----------------------
	//#define Com_NRF24						1
	//#define Com_RFM73						1
    #define Com_RFM75                       1
    
    //--------------- IO Interface --------------------    
    #define PSOC4_Serial                    1
    //#define PSOC5_Serial                    1
        
    //#define PSOC5_USB                       1
    //#define PSOC5_USB_In_EP                 1
    //#define PSOC5_USB_Out_EP                2

	//#define Arduino_SPI_CLK			    12
	//#define Arduino_SPI_MISO				11
	//#define Arduino_SPI_MOSI				10

    //--------------- IO Setting --------------------    
    #define MasterSPI_Native                1
    //#define MasterSPI_Pin                   1
    //#define MasterSPI_MultiSlave            1 
#endif
