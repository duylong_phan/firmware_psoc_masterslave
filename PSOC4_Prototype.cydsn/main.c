#include "Config.h"
#include "../External References/Definitions.h"
#include "../External References/Queue.h"
#include "../External References/SubFunctions.h"
#include "../External References/HeaderPackage.h"
#include "../External References/RFM7X.h"

/*----Switch Interface-----------------
in Config.h
Comment the Interface which is not used
*/

/*----Switch Component: RF Module-------
in Config.h
+ Comment the Component which is not used: Com_NRF24 or Com_RFM73
*/

//--------------------------Struct, Union----------------------

struct TaskHandler
{
    unsigned char CommandCount;
    int TickCount;
};

struct SlavePressInfo
{
    unsigned char ID;
    unsigned char Index;
};

//--------------------------Buffer--------------------------
unsigned char __stateContainer[Size_StateContainer];
unsigned char __headerContainer[Size_HeaderContainer];


//--------------------------Setting----------------------------
static const unsigned char SettingBuffer[Size_SettingBuffer] = 
{
    0x14, 0x34, 0x43, 0x10, 0x10,
    0x01, 0x34, 0x43, 0x10, 0x10,
    0x01, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00
};

const unsigned char *Setting_RF_Channel = &SettingBuffer[Setting_Master_RF_Channel];
const unsigned char *Setting_RF_Rx_Address = &SettingBuffer[Setting_Master_RF_Rx_Address];
const unsigned char *Setting_RF_Tx_Address = &SettingBuffer[Setting_Master_RF_Tx_Address];

//---------------------------Global Variable--------------------
struct Queue stateQueue;
struct TaskHandler taskHandler;
struct RfHandler rfHandler;
struct HeaderPackage inputPackage;
struct SlavePressInfo slavePressInfo[Size_SlaveAmount];

#ifdef PSOC5_USB
    struct UsbHandler usbHandler;
#endif

//---------------------------Function Prototype-----------------

void Do_Command_Start();
void Do_Command_Stop();
void Do_Command_RF_Transmit();
void Do_Command_Read_MasterSetting();
void Do_Command_Write_MasterSetting();

void On_Dummy_Received();
void On_Press_Received();

void SubTask_RfTrans();

//----------------------------Main Method-----------------------
 int main()
{  
    Initialize_HW();
    Initialize_SW();
    Initialize_Parameters();
    for(;;)
    {
        Single_Operation();    
    }
}

CY_ISR(Triggered_RF_Inter)
{
    On_RF_Inter();
}

CY_ISR(Triggered_Timer_Inter)
{
    On_Timer_Inter();
}

//---------------------------Initialize HW, SW-------------------
//Initialize Hardware Components and APIs
void Initialize_HW()
{    
    //Important => always call this
    CyGlobalIntEnable; 
        
    //Enable PC Communication Interface
    #if defined(PSOC4_Serial) || defined(PSOC5_Serial)
        Serial_Start();
    #endif
    #ifdef PSOC5_USB
        USBFS_Start(0, USBFS_5V_OPERATION);
        while(!USBFS_GetConfiguration()) {}    
        USBFS_EnableOutEP(PSOC5_USB_Out_EP);
    #endif    
    
    //Enable SPI => RF
    SPIM_Start();
    RFM7X_Start();   
    RFM7X_SetChannel(*Setting_RF_Channel);
    RFM7X_SetAddress_Tx(Setting_RF_Rx_Address);
    RFM7X_SetAddress_Rx(Setting_RF_Tx_Address);             
    RFM7X_PowerDown();
    
    //Enable RF Interrrupt
    RF_Inter_StartEx(Triggered_RF_Inter);
    
    //Enable Timer Interrupt
    Timer_Inter_StartEx(Triggered_Timer_Inter);    
}

//Initialize Software components
void Initialize_SW()
{     
    int i = 0;
    
    //-----handler----
    taskHandler.TickCount = 0;
    taskHandler.CommandCount = 0;
    
    rfHandler.Pipe = 0;
    rfHandler.Length = 0;
    rfHandler.LostCount = 0;
    rfHandler.Mode = RF_Off;
    Sub_ResetArray_Unsigned(rfHandler.TransmitPackage, Size_RF_Package);
    Sub_ResetArray_Unsigned(rfHandler.ReceivePackage, Size_RF_Package);    
             
    //----Queue----
    Queue_Initialize(&stateQueue, __stateContainer, Size_StateContainer, sizeof(unsigned char));
    
    //----Package----
    HeaderPackage_Intialize(&inputPackage, __headerContainer);
    #ifdef PSOC5_USB
        usbHandler.Length = 0;
        Sub_ResetArray_Unsigned(usbHandler.TransmitPackage, PSOC5_USB_Size_Package);
        Sub_ResetArray_Unsigned(usbHandler.ReceivePackage, PSOC5_USB_Size_Package);    
        usbHandler.Length = &usbHandler.TransmitPackage[PSOC5_USB_Index_Length];
        HeaderPackage_SetUsbHandler(&usbHandler);
    #endif
    
    //SlavePressInfo
    for(i = 0; i < Size_SlaveAmount; i++)
    {
        slavePressInfo[i].ID = i;
        slavePressInfo[i].Index = 0;
    }
}

void Initialize_Parameters()
{
    //Start Timer
    Timer_PWM_Start(); 
}

//Life circle of the Microcontroller
void Single_Operation()
{
    unsigned char state = (stateQueue.Count > 0) ? *(unsigned char*)Queue_Dequeue(&stateQueue) : State_Wait_Input;
    switch(state)
    {
        case State_RF_Interrupt:        Do_RF_Interrupt();      break;
        case State_RF_Receive:          Do_RF_Receive();        break;
        case State_RF_Transmit:         Do_RF_Transmit();       break;
        case State_Process_Input:       Do_Process_Input();     break;
        case State_Generate_Output:     Do_Generate_Output();   break;
        case State_Task:                Do_Task();              break;
        case State_Wait_Input:
        default:                        Do_Wait_Input();        break;
    }
}

//--------------------Interrupt Method------------------
void On_RF_Inter()
{
    unsigned char nextState = State_RF_Interrupt;
    Queue_Enqueue(&stateQueue, &nextState);
}

void On_Timer_Inter()
{
    //Time tracker
    taskHandler.TickCount++;    
    if(taskHandler.TickCount >= TickCount_Minute)
    {
        taskHandler.TickCount = 0;
    }
    
    //Enable to check RF status
    rfHandler.CheckInput = 1;
    
    //Notify PC Command received
    if(taskHandler.CommandCount > 0)
    {
        taskHandler.CommandCount++;
        if(taskHandler.CommandCount >= 50)
        {
            taskHandler.CommandCount = 0;
            LED_1_Write(!LED_1_Read());
        }
    }
}

//---------------------------Operation Method--------------------
//On RF Interrupt, or requested
void Do_RF_Interrupt()
{
    unsigned char hasIn, hasOut, hasLost;   
    
    //Get, clear Interrupt
    Sub_DelayUs(50);
    RFM7X_GetInterrupt(&hasIn, &hasOut, &hasLost);
    
    //When transmision completed
    if(hasOut)
    {        
        LED_2_Write(0);
    }  
    //Only when RF is in Transmitter mode
    if(rfHandler.Mode == RF_Transmitter)
    {
        rfHandler.Mode = RF_Receiver;
        RFM7X_Mode_Rx();
    }
}

//Read Package from Slave
void Do_RF_Receive()
{    
    unsigned char nextState = State_Generate_Output;
    unsigned char hasData = 0;
        
    //alway check if there is an available Package, before process further
    hasData = RFM7X_Receive(rfHandler.ReceivePackage, &rfHandler.Length, &rfHandler.Pipe);
    if(hasData)
    {
        Queue_Enqueue(&stateQueue, &nextState);         
        LED_2_Write(0);
    } 
}

//Transmit package to Slave
void Do_RF_Transmit()
{    
    //To Tx, if current mode is Rx
    if(rfHandler.Mode != RF_Transmitter)
    {
        rfHandler.Mode = RF_Transmitter;        
        RFM7X_Mode_Tx();
    }
    //Clear Interrupt
    else
    {
        RFM7X_ClearInterrupt();
    }   
    //Before transmitting package, always clear Interrupt, explicit or indirect in RFM7X_Mode_Tx
    //to allow transmision, ignore current State of RF Module
    RFM7X_Transmit(rfHandler.TransmitPackage, Size_RF_Package);
    LED_2_Write(1);
}

//Process Input Package from PC
void Do_Process_Input()
{    
    unsigned char hasCommand = 1;
        
    switch(inputPackage.Type)
    {
        case PC_Start:                  Do_Command_Start();                         break;
        case PC_Stop:                   Do_Command_Stop();                          break;
        case PC_RF_Transmit:            Do_Command_RF_Transmit();                   break;
        case PC_Read_MasterSetting:     Do_Command_Read_MasterSetting();            break;
        case PC_Write_MasterSetting:    Do_Command_Write_MasterSetting();           break;        
        default:                        hasCommand = 0;                             break;
    }  
    
    //Notify Input from PC is received => Toggle LED_1 in Timer_Inter
    if(hasCommand)
    {
        taskHandler.CommandCount = 1;
        LED_1_Write(!LED_1_Read());
    }
}

//Generate Package in order to transmit to PC
void Do_Generate_Output()
{
    unsigned char hasHandler = 1;
    
    //Generate base on TaskID
    switch(rfHandler.ReceivePackage[Index_TaskID])
    {
        case TaskID_Dummy:              On_Dummy_Received();        break;    
        case TaskID_Press:              On_Press_Received();        break;             
        default:                        hasHandler = 0;             break;  
    }
    
    //Package from slave is ignored => Transceiver error, or package format isn't matched
    if(hasHandler==0)
    {
        LED_Write(!LED_Read());
    }
}

//Do specific Task for each Task on requesting
void Do_Task()
{
    
}

//Wait input from PC, or Slave
void Do_Wait_Input()
{
    unsigned char nextState = State_Wait_Input; 
    
    if(rfHandler.CheckInput)
    {
        rfHandler.CheckInput = 0;
        //When Input from Slave => return when available
        if(rfHandler.Mode == RF_Receiver && RFM7X_HasData())
        {
            nextState = State_RF_Receive;
            Queue_Enqueue(&stateQueue, &nextState);  
            LED_2_Write(1); 
            return;
        }
    }
    
    //when input from PC is available, or queued data is available => return when available
    if(HasInput() || inputPackage.Queue.Count > 0)
    {
        if(HeaderPackage_Parse(&inputPackage))
        {
            nextState = State_Process_Input;
            Queue_Enqueue(&stateQueue, &nextState);
            return;
        }
    }    
    
    //nothing to do
    //Important => CPU will process data in Background
    Sub_DelayUs(10);
}

//Check and Get input value
unsigned char HasInput()
{
    unsigned char hasInput = 0;    
    unsigned char tmpValue = 0;
    
    #ifdef PSOC4_Serial        
        hasInput = Serial_SpiUartGetRxBufferSize();
        while(Serial_SpiUartGetRxBufferSize() > 0)
        {
            tmpValue = Serial_UartGetByte();
            Queue_Enqueue(&inputPackage.Queue, &tmpValue);
        }
    #endif
    
    #ifdef PSOC5_Serial
        hasInput = Serial_GetRxBufferSize();
        while(Serial_GetRxBufferSize() > 0)
        {
            tmpValue = Serial_GetByte();
            Queue_Enqueue(&inputPackage.Queue, &tmpValue);
        }
    #endif
    
    #ifdef PSOC5_USB
        int length = 0;
        if(USBFS_GetEPState(PSOC5_USB_Out_EP) == USBFS_OUT_BUFFER_FULL)
        {
            length = USBFS_GetEPCount(PSOC5_USB_Out_EP);
            USBFS_ReadOutEP(PSOC5_USB_Out_EP, usbHandler.ReceivePackage, length);            
            for(tmpValue = 0; tmpValue < length; tmpValue++)
            {
                Queue_Enqueue(&inputPackage.Queue, &usbHandler.ReceivePackage[tmpValue]);
            }
            hasInput = 1;
        }        
    #endif
    
    return hasInput;
}

//-----------------------PC Commands-------------------------
void Do_Command_Start()
{
    LED_1_Write(1);  
    RFM7X_Mode_Rx();
    rfHandler.Mode = RF_Receiver;
}

void Do_Command_Stop()
{
    LED_1_Write(0);
    RFM7X_PowerDown();
    rfHandler.Mode = RF_Off;;
}

void Do_Command_Read_MasterSetting()
{
    unsigned char package[64];
    //Assign Package
    package[Index_PC_Command] = PC_Read_MasterSetting;
    package[Index_PC_Size] = Size_SettingBuffer;    
    memcpy(&package[Index_Payload], SettingBuffer, Size_SettingBuffer);
    
    //Response to PC
    HeaderPackage_WritePackage(package, Size_SettingBuffer + 2);
    HeaderPackage_Flush();
}

void Do_Command_Write_MasterSetting()
{
    unsigned char package[3];
    
    //Write into Flash Memory
    EEPROM_Start();
    EEPROM_Write(inputPackage.PayLoad, SettingBuffer, Size_SettingBuffer);
    EEPROM_Stop();
    
    //Response => Successful
    package[Index_PC_Command] = PC_Command_Done;
    package[Index_PC_Size] = 1;
    package[Index_Payload] = PC_Write_MasterSetting;
    HeaderPackage_WritePackage(package, 3);
    HeaderPackage_Flush();
    
    //Wait to transmit package, and reset device
    Sub_DelayMs(100);
    CySoftwareReset();
}

void Do_Command_RF_Transmit()
{
    unsigned char nextState = State_RF_Transmit;
    memcpy(rfHandler.TransmitPackage, inputPackage.PayLoad, inputPackage.Length);
    Queue_Enqueue(&stateQueue, &nextState);
}

//-------------------Receive Package from slave: Task specific-----------------
void On_Dummy_Received()
{
    LED_Write(!LED_Read());
}

void On_Press_Received()
{
    unsigned char hasPackage = 0;
    unsigned char package[32];    
    
    //------------------------Command/Status Package----------------------------
    switch(rfHandler.ReceivePackage[Index_Type])
    {
        case Type_Press_DeviceStatus:
        package[Index_PC_Command] = PC_Press_Status;
        package[Index_PC_Size] = 9;               
        hasPackage = 1;
        break;
        
        case Type_Press_ReadSetting:
        package[Index_PC_Command] = PC_Read_SlaveSetting;
        package[Index_PC_Size] = 30;        
        hasPackage = 1;
        break;
    }    
    
    if(hasPackage)    
    {
        memcpy(&package[Index_Payload], &rfHandler.ReceivePackage[Index_Payload], package[Index_PC_Size]);
        HeaderPackage_WritePackage(package, package[Index_PC_Size] + 2);
        HeaderPackage_Flush();
        return;
    }
      
    //-------------------------Data Package---------------------------------
    //Plier was pressed
    if((rfHandler.ReceivePackage[Index_Type] & Type_Press_Pressed) != 0)
    {
        //First Index is ignored (0 value) && index is matched => only one clamping action
        //In case, master received the slave's package, but slave has not received acknowledge package from master
        if(slavePressInfo[rfHandler.ReceivePackage[Pos_Press_ID]].Index != 0 &&
           rfHandler.ReceivePackage[Pos_Press_PackageIndex] == slavePressInfo[rfHandler.ReceivePackage[Pos_Press_ID]].Index)
        {
            rfHandler.ReceivePackage[Pos_Press_Amount] = 1;            
        }      
        //update Index for slave's next package index
        slavePressInfo[rfHandler.ReceivePackage[Pos_Press_ID]].Index = rfHandler.ReceivePackage[Pos_Press_PackageIndex] + 1;
        
        //Generate package
        package[Index_PC_Command] = PC_Press_Pressed;
        package[Index_PC_Size] = 2;
        memcpy(&package[Index_Payload], &rfHandler.ReceivePackage[Pos_Press_ID], package[Index_PC_Size]);       
        HeaderPackage_WritePackage(package, package[Index_PC_Size] + 2);
        HeaderPackage_Flush();
    }
    
    //Sample Curve for debug
    if((rfHandler.ReceivePackage[Index_Type] & Type_Press_Debug) != 0)
    {        
        package[Index_PC_Command] = PC_Press_Sample;
        package[Index_PC_Size] = Size_Press_Sample;
        memcpy(&package[Index_Payload], &rfHandler.ReceivePackage[Pos_Press_Sample], package[Index_PC_Size]);       
        HeaderPackage_WritePackage(package, package[Index_PC_Size] + 2);
        HeaderPackage_Flush();
    }
}